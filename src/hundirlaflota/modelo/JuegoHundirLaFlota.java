/*
 * Copyright (C) 2013 Ignacio Calvo <ignacio.calvo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hundirlaflota.modelo;

import hundirlaflota.modelo.agentes.ColocadorHumano;
import hundirlaflota.modelo.tablero.TableroBarcos;
import tablero.modelo.Universo;
import hundirlaflota.modelo.agentes.ColocadorSimple;
import hundirlaflota.modelo.agentes.DisparadorHumano;
import hundirlaflota.modelo.agentes.DisparadorPractica;
import hundirlaflota.modelo.agentes.ProveedorCoordenadasAleatorio;
import hundirlaflota.modelo.fases.ColocacionFlotas;
import hundirlaflota.modelo.fases.Disparos;
import java.util.ArrayList;
import java.util.Collection;
import tablero.modelo.Fase;
import tablero.modelo.Juego;

/**
 *
 * @author Ignacio Calvo <ignacio.calvo@gmail.com>
 */
public class JuegoHundirLaFlota 
extends Juego<JugadorHundirLaFlota, TableroBarcos> {

    @Override
    public Universo<JugadorHundirLaFlota, TableroBarcos> getUniverso() {
        Universo<JugadorHundirLaFlota, TableroBarcos> universo;

        TableroBarcos tablero1, tablero2;
        tablero1 = new TableroBarcos(
                Configuracion.getAltoNuevoTablero(),
                Configuracion.getAnchoNuevoTablero());
        tablero2 = new TableroBarcos(
                Configuracion.getAltoNuevoTablero(),
                Configuracion.getAnchoNuevoTablero());
        
        JugadorHundirLaFlota j1 = new JugadorHundirLaFlota("J1");
        JugadorHundirLaFlota j2 = new JugadorHundirLaFlota("J2");

        universo = new Universo<>(j1, tablero1, j2, tablero2);

        j1.setColocador(new ColocadorHumano(j1.getNombre()));
        j2.setColocador(new ColocadorSimple(j2.getNombre(), new ProveedorCoordenadasAleatorio()));
 
        j1.setDisparador(new DisparadorHumano(j1.getNombre()));
        j2.setDisparador(new DisparadorPractica(j2.getNombre(), new ProveedorCoordenadasAleatorio()));
        
        return universo;
    }

    @Override
    protected Collection<Fase> getFases(Universo<JugadorHundirLaFlota, TableroBarcos> universo) {
        ArrayList<Fase> fases = new ArrayList<>();
        
        fases.add(new ColocacionFlotas(universo));
        fases.add(new Disparos(universo));
        return fases;
    }
    
}
