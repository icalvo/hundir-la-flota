/*
 * Copyright (C) 2013 Ignacio Calvo <ignacio.calvo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hundirlaflota.modelo;

/**
 *
 * @author Ignacio Calvo <ignacio.calvo@gmail.com>
 */
public enum Direccion {
    /**
     * Hacia la derecha del tablero
     */
    ESTE ("E", "este"),
    /**
     * Hacia abajo del tablero
     */
    SUR ("S", "sur"),
    /**
     * Hacia la izquierda del tablero
     */
    OESTE ("O", "oeste"),
    /**
     * Hacia arriba del tablero
     */
    NORTE ("N", "norte");

    private final String abrev;
    private final String nombre;

    private Direccion(String abrev, String nombre) {
        this.abrev = abrev;
        this.nombre = nombre;
    }

    public String getAbrev() {
        return abrev;
    }

    public String getNombre() {
        return nombre;
    }
    
    /**
     * 
     * @return Dirección a la derecha de la actual (sentido de las agujas del
     * reloj)
     */
    public Direccion derecha() {
        switch (this) {
            case NORTE:
                return ESTE;
            case ESTE:
                return SUR;
            case SUR:
                return OESTE;
            case OESTE:
                return NORTE;
        }
        throw new IllegalArgumentException();
    }
        
    public Direccion inversa() {
        if (this == null) {
            return null;
        }
        
        switch (this) {
            case NORTE:
                return SUR;
            case ESTE:
                return OESTE;
            case SUR:
                return NORTE;
            case OESTE:
                return ESTE;
        }
        throw new IllegalArgumentException();
    }

    /**
     *
     * @param tamanio
     * @return
     */
    public static Direccion fromAbrev(String abrev) {
        for (Direccion d : Direccion.values()) {
            if (abrev == null ? d.getAbrev() == null : abrev.equals(d.getAbrev())) {
                return d;
            }
        }
        return null;
    }
    
}
