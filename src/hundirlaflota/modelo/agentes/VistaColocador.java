/*
 * Copyright (C) 2013 Ignacio Calvo <ignacio.calvo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hundirlaflota.modelo.agentes;

import hundirlaflota.modelo.Flota;

/**
 *
 * @author Ignacio Calvo <ignacio.calvo@gmail.com>
 */
public class VistaColocador {
    private int altoTablero;
    private int anchoTablero;
    private Flota flota;

    public VistaColocador(int altoTablero, int anchoTablero, Flota flota) {
        this.altoTablero = altoTablero;
        this.anchoTablero = anchoTablero;
        this.flota = flota;
    }

    public int getAltoTablero() {
        return altoTablero;
    }

    public int getAnchoTablero() {
        return anchoTablero;
    }

    public Flota getFlota() {
        return flota;
    }
    
}
