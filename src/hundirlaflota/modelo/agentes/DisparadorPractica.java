/*
 * Copyright (C) 2013 Ignacio Calvo <ignacio.calvo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hundirlaflota.modelo.agentes;

import hundirlaflota.modelo.tablero.Coordenada;
import hundirlaflota.modelo.Direccion;
import static hundirlaflota.modelo.Direccion.NORTE;
import hundirlaflota.modelo.tablero.Tablero;
import hundirlaflota.modelo.tablero.TipoCasilla;
import static hundirlaflota.modelo.tablero.TipoCasilla.SIN_DISPARAR;
import static hundirlaflota.modelo.tablero.TipoCasilla.AGUA;
import static hundirlaflota.modelo.tablero.TipoCasilla.TOCADO;
import static hundirlaflota.modelo.tablero.TipoCasilla.HUNDIDO;
import tablero.modelo.Agente;

/**
 * El disparador pedido en la práctica dispara aleatoriamente hasta acertar;
 * en ese momento buscará la dirección del barco (horizontal o vertical),
 * disparando siempre en el orden: casilla superior, derecha, inferior e 
 * izquierda. Una vez determinada la dirección, seguirá disparando en uno
 * de los sentidos hasta que hunda el barco o haga agua, y en caso de agua
 * seguirá en el sentido opuesto. Luego vuelve al inicio.
 * @author Ignacio Calvo <ignacio.calvo@gmail.com>
 */
public class DisparadorPractica extends Disparador {

    private ProveedorCoordenadas proveedorCoords;
    
    private Coordenada primerTocado;
    private Coordenada cabeza;
    private Direccion direccion;
    
    private Estado estado;
    /**
     *
     * @param jugador
     */
    public DisparadorPractica(String jugador, ProveedorCoordenadas proveedorCoords) {
        super(jugador);
        this.estado = Estado.SIN_BARCO_LOCALIZADO;
        this.proveedorCoords = proveedorCoords;
    }
    
    private enum Estado {
        SIN_BARCO_LOCALIZADO,
        BARCO_LOCALIZADO_SIN_DIRECCION,
        BARCO_LOCALIZADO_CON_DIRECCION,
        BARCO_LOCALIZADO_CON_DIRECCION_INVERSA
    }
    
    private Estado getEstado() {
        return estado;
    }
    
    private boolean esDisparable(Coordenada coord, VistaDisparador vista) {
        Tablero tablero = vista.getTableroEnemigo();

        if (!tablero.esCoordenadaValida(coord)) {
            System.out.format("La coordenada %s es inválida\n", coord.toA0String());
        }
        else if (!(tablero.casillaEn(coord).getTipo() == TipoCasilla.SIN_DISPARAR)) {
            System.out.format("La coordenada %s ya fue disparada\n", coord.toA0String());
        }
        else {
            System.out.format("La coordenada %s es disparable\n", coord.toA0String());
        }
        return 
            tablero.esCoordenadaValida(coord) &&
            tablero.casillaEn(coord).getTipo() == TipoCasilla.SIN_DISPARAR;
    }
    
    /**
     * Obtiene la siguiente dirección válida de la dada. Si a partir de coord,
     * la casilla en la dirección dada es válida, se devuelve la misma. Si no,
     * se recorre en sentido horaro hasta encontrar alguna.
     * @param dir
     * @param coord
     * @param vista
     * @return 
     */
    private Direccion siguienteDisparable(Direccion dir, Coordenada coord, VistaDisparador vista) {
        Tablero tablero = vista.getTableroEnemigo();

        assert tablero.esCoordenadaValida(coord);

        Direccion result = dir;
        System.out.format("Probando hacia %s (%s)... ", result, coord.adelante(result));
        while (!esDisparable(coord.adelante(result), vista)) {
            result = result.derecha();
            System.out.format("Probando hacia %s (%s)... ", result, coord.adelante(result));
        }
        return result;
    }
    
    
    @Override
    public Coordenada nuevaJugada(VistaDisparador vista) {
        Coordenada resultado = null;

        switch (getEstado()) {
            case SIN_BARCO_LOCALIZADO:
                // No tenemos barco localizado
                resultado = nuevaJugadaAleatoria(vista);
                break;
            case BARCO_LOCALIZADO_SIN_DIRECCION:
            case BARCO_LOCALIZADO_CON_DIRECCION:
            case BARCO_LOCALIZADO_CON_DIRECCION_INVERSA:
                resultado = cabeza.adelante(direccion);
                break;
            default:
                throw new IllegalArgumentException();
        }
        assert esDisparable(resultado, vista);
        
        return resultado;
    }
    
    private Coordenada nuevaJugadaAleatoria(VistaDisparador vista) {
        return proveedorCoords.nuevaCoordenada(vista.getTableroEnemigo());
    }
    
    private TipoCasilla getTipo(VistaDisparador vista, Coordenada coord) {
        return vista.getTableroEnemigo().casillaEn(coord).getTipo();
    }

    @Override
    public void notificar(
            Coordenada coord, 
            Agente<Coordenada, VistaDisparador> agente,
            VistaDisparador vista) {
        
        if (agente != this) {
            return;
        }
        
        TipoCasilla resultado = getTipo(vista, coord);

        Estado nuevoEstado = estado;
        
        assert resultado != SIN_DISPARAR;

        switch (getEstado()) {
            case SIN_BARCO_LOCALIZADO:
                // No tenemos barco localizado
                switch (resultado) {
                    case TOCADO:
                        // Barco localizado: marcamos primera coord tocada
                        primerTocado = coord;
                        cabeza = coord;
                        direccion = siguienteDisparable(NORTE, coord, vista);
                        nuevoEstado = Estado.BARCO_LOCALIZADO_SIN_DIRECCION;
                        break;
                    case AGUA:
                        break;
                    case HUNDIDO:
                        // Barco hundido al primer tiro, sólo debe pasar si
                        // jugamos con barcos de tamaño 1. Reseteamos
                        primerTocado = null;
                        cabeza = null;
                        direccion = null;
                        nuevoEstado = Estado.SIN_BARCO_LOCALIZADO;
                        break;
                }
                break;
            case BARCO_LOCALIZADO_SIN_DIRECCION:
                // Tenemos barco pero no sabemos su dirección
                switch (resultado) {
                    case TOCADO:
                        // Establecemos dirección
                        
                        if (esDisparable(coord.adelante(direccion), vista)) {
                            // Seguimos igual
                            cabeza = coord;
                            nuevoEstado = Estado.BARCO_LOCALIZADO_CON_DIRECCION;
                        }
                        else {
                            // Cambiamos dirección marcando como casilla de comienzo
                            // la primera tocada del barco
                            cabeza = primerTocado;
                            direccion = direccion.inversa();
                            nuevoEstado = Estado.BARCO_LOCALIZADO_CON_DIRECCION_INVERSA;
                        }
                        break;
                    case AGUA:
                        // Probamos siguiente dirección
                        direccion = siguienteDisparable(direccion.derecha(), cabeza, vista);
                        break;
                    case HUNDIDO:
                        // Reseteamos
                        primerTocado = null;
                        cabeza = null;
                        direccion = null;
                        nuevoEstado = Estado.SIN_BARCO_LOCALIZADO;
                        break;
                }
                break;

            case BARCO_LOCALIZADO_CON_DIRECCION:
                // Tenemos barco y lo estamos disparando en la dirección
                // encontrada
                switch (resultado) {
                     case TOCADO:
                        if (esDisparable(coord.adelante(direccion), vista)) {
                            // Seguimos igual
                            cabeza = coord;
                        }
                        else {
                            // Cambiamos dirección marcando como casilla de comienzo
                            // la primera tocada del barco
                            cabeza = primerTocado;
                            direccion = direccion.inversa();
                            nuevoEstado = Estado.BARCO_LOCALIZADO_CON_DIRECCION_INVERSA;
                        }

                         break;
                     case AGUA:
                         // Cambiamos dirección marcando como casilla de comienzo
                         // la primera tocada del barco
                         cabeza = primerTocado;
                         direccion = direccion.inversa();
                         nuevoEstado = Estado.BARCO_LOCALIZADO_CON_DIRECCION_INVERSA;
                         break;
                     case HUNDIDO:
                         // Reseteamos
                         primerTocado = null;
                         cabeza = null;
                         direccion = null;
                         nuevoEstado = Estado.SIN_BARCO_LOCALIZADO;
                         break;
                 }
                 break;
            case BARCO_LOCALIZADO_CON_DIRECCION_INVERSA:
                // Tenemos barco y lo estamos disparando en la dirección opuesta a la
                // encontrada
                switch (resultado) {
                    case TOCADO:
                        // Seguimos igual
                        cabeza = coord;
                        break;
                    case AGUA:
                        // ESTO NO DEBERÍA PASAR
                        // ¿Quizá hemos permitido barcos pegados?
                        // Podemos lanzar excepción; preferimos continuar como
                        // si fuera hundido.
                        System.out.println("AGUA CON BARCO_LOCALIZADO_CON_DIRECCION_INVERSA!!");
                    case HUNDIDO:
                        // Reseteamos
                        primerTocado = null;
                        cabeza = null;
                        direccion = null;
                        nuevoEstado = Estado.SIN_BARCO_LOCALIZADO;
                        break;
                }
        }        
        
        if (nuevoEstado == Estado.BARCO_LOCALIZADO_CON_DIRECCION) {
            assert esDisparable(cabeza.adelante(direccion), vista);
        }
        if (nuevoEstado == Estado.BARCO_LOCALIZADO_CON_DIRECCION_INVERSA) {
            assert esDisparable(cabeza.adelante(direccion), vista);
        }
        
        if (!estado.equals(nuevoEstado)) {
            System.out.format("%s -> %s\n", estado, nuevoEstado);
        }
        
        estado = nuevoEstado;
    }
        
}
