/*
 * Copyright (C) 2013 Ignacio Calvo <ignacio.calvo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hundirlaflota.modelo.agentes;

import hundirlaflota.modelo.JugadorHundirLaFlota;
import hundirlaflota.modelo.tablero.TableroOculto;
import hundirlaflota.modelo.tablero.TableroBarcos;
import hundirlaflota.modelo.tablero.TableroSoloLectura;
import tablero.modelo.Universo;

/**
 *
 * @author Ignacio Calvo <ignacio.calvo@gmail.com>
 */
public class VistaDisparador {
    TableroSoloLectura tableroPropio;
    TableroOculto tableroEnemigo;

    /**
     *
     * @param jugador
     * @param universo
     */
    public VistaDisparador(JugadorHundirLaFlota jugador, Universo<JugadorHundirLaFlota, TableroBarcos> universo) {
        this.tableroPropio = new TableroSoloLectura(universo.getDatosPropios(jugador));
        this.tableroEnemigo = new TableroOculto(universo.getDatosEnemigo(jugador));
    }

    public VistaDisparador(TableroBarcos tableroEnemigo) {
        this.tableroPropio = null;
        this.tableroEnemigo = new TableroOculto(tableroEnemigo);
    }
    
    VistaDisparador(Universo universo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @return
     */
    public TableroSoloLectura getTableroPropio() {
        return tableroPropio;
    }

    /**
     *
     * @return
     */
    public TableroOculto getTableroEnemigo() {
        return tableroEnemigo;
    }
    
}
