/*
 * Copyright (C) 2013 Ignacio Calvo <ignacio.calvo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hundirlaflota.modelo.agentes;

import hundirlaflota.modelo.Flota;
import hundirlaflota.modelo.tablero.TableroBarcos;
import tablero.modelo.Agente;

/**
 * El agente colocador devuelve jugadas para la fase de colocación de barcos
 * ({@link hundirlaflota.modelo.fases.ColocacionFlotas}). La jugada es única
 * y consiste en una flota con las posiciones y dirección de cada buque.
 * Se proporciona como vista un tablero de sólo lectura, que estará vacío.
 * El agente debe crear un tablero de borrador usando las dimensiones de la
 * vista, por ejemplo:
 * TableroBarcos tablero = new TableroBarcos(vista.getAlto(), vista.getAncho());
 * Este nuevo tablero tiene la función {@link TableroBarcos#ponerBarco(hundirlaflota.modelo.Barco, hundirlaflota.modelo.Coordenada, hundirlaflota.modelo.Direccion) ponerBarco},
 * que nos permite ensayar posicionamientos erróneos, devolviéndonos en tales 
 * casos una información sobre el error cometido.
 * Aun así tenemos la potestad de devolver una flota errónea (por ejemplo
 * sin barcos, o con menos barcos o más de los requeridos, o mal posicionados);
 * en tal caso la función {@link hundirlaflota.modelo.fases.ColocacionFlotas#aplicarJugada(hundirlaflota.modelo.TableroBarcos, tablero.modelo.Agente)},
 * que se usará para aplicar nuestra jugada, detectará el error y el agente
 * deberá volver a suministrar una nueva jugada.
 * @author Ignacio Calvo <ignacio.calvo@gmail.com>
 */
public abstract class Colocador implements Agente<Flota, VistaColocador> {
    private String nombreJugador;
   
    /**
     *
     * @param jugador
     * @param flota
     */
    protected Colocador(String nombreJugador) {
        this.nombreJugador = nombreJugador;
    }

    @Override
    public String getNombreJugador() {
        return nombreJugador;
    }
  
    @Override
    public abstract Flota nuevaJugada(VistaColocador vista);
   
    @Override
    public abstract void notificar(
            Flota jugada, 
            Agente<Flota, VistaColocador> agente,
            VistaColocador vista);
       
}
