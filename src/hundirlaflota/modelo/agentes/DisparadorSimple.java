/*
 * Copyright (C) 2013 Ignacio Calvo <ignacio.calvo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hundirlaflota.modelo.agentes;

import hundirlaflota.modelo.tablero.Coordenada;
import tablero.modelo.Agente;

/**
 * El disparador aleatorio escoge siempre una coordenada aleatoria, aunque
 * haya acertado en el disparo anterior.
 * @author Ignacio Calvo <ignacio.calvo@gmail.com>
 */
public class DisparadorSimple extends Disparador {

    private ProveedorCoordenadas proveedorCoords;
    
    /**
     *
     * @param jugador
     */
    public DisparadorSimple(String jugador, ProveedorCoordenadas proveedorCoords) {
        super(jugador);
        
        this.proveedorCoords = proveedorCoords;
    }
    
    @Override
    public Coordenada nuevaJugada(VistaDisparador vista) {
        return proveedorCoords.nuevaCoordenada(vista.getTableroEnemigo());
    }

    @Override
    public void notificar(
            Coordenada coord, 
            Agente<Coordenada, VistaDisparador> agente,
            VistaDisparador vista) {
    }
    
}
