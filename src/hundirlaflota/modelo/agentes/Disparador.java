/*
 * Copyright (C) 2013 Ignacio Calvo <ignacio.calvo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hundirlaflota.modelo.agentes;

import hundirlaflota.modelo.tablero.Coordenada;
import tablero.modelo.Agente;

/**
 * El disparador es el agente de la fase de Disparos. La jugada es una
 * coordenada.
 * @author Ignacio Calvo <ignacio.calvo@gmail.com>
 */
public abstract class Disparador implements Agente<Coordenada, VistaDisparador> {
    
    private String jugador;
    
    /**
     * 
     * @param jugador 
     */
    protected Disparador(String jugador) {
        this.jugador = jugador;
    }
    
    @Override
    public abstract Coordenada nuevaJugada(VistaDisparador vista);

    @Override
    public String getNombreJugador() {
        return jugador;
    }

    /**
     *
     * @return
     */
    public String getNombre() {
        return "disparador de " + jugador;
    }

    @Override
    public abstract void notificar(
            Coordenada jugada, 
            Agente<Coordenada, VistaDisparador> agente,
            VistaDisparador vista);
    
}
