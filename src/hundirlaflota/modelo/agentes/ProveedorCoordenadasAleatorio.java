/*
 * Copyright (C) 2013 Ignacio Calvo <ignacio.calvo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hundirlaflota.modelo.agentes;

import hundirlaflota.modelo.Configuracion;
import hundirlaflota.modelo.tablero.Coordenada;
import hundirlaflota.modelo.tablero.Tablero;
import static hundirlaflota.modelo.tablero.TipoCasilla.SIN_DISPARAR;
import java.util.Random;

/**
 *
 * @author Ignacio Calvo <ignacio.calvo@gmail.com>
 */
public class ProveedorCoordenadasAleatorio implements ProveedorCoordenadas {

    private Random rnd = new Random(Configuracion.getRandomSeed());

    @Override
    public Coordenada nuevaCoordenada(Tablero tableroEnemigo) {
        // TODO: Implementación más eficiente, podemos generar un número
        // aleatorio entre 1 y el número de casillas sin disparar del tablero.
        Coordenada coord;
        boolean jugadaEncontrada;
        do {
            int x = rnd.nextInt(tableroEnemigo.getAlto());
            int y = rnd.nextInt(tableroEnemigo.getAncho());
            coord = new Coordenada(x, y);
            jugadaEncontrada = tableroEnemigo.casillaEn(coord).getTipo() == SIN_DISPARAR;
        } while(!jugadaEncontrada);
        return coord;
    }
    
    
}
