/*
 * Copyright (C) 2013 Ignacio Calvo <ignacio.calvo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hundirlaflota.modelo.agentes;

import hundirlaflota.interfaz.IO;
import hundirlaflota.modelo.tablero.Barco;
import hundirlaflota.modelo.ColocacionBarco;
import hundirlaflota.modelo.tablero.Coordenada;
import hundirlaflota.modelo.Direccion;
import hundirlaflota.modelo.Flota;
import hundirlaflota.modelo.ResultadoPonerBarco;
import hundirlaflota.modelo.tablero.TableroBarcos;
import static hundirlaflota.modelo.ResultadoPonerBarco.COLISION;
import static hundirlaflota.modelo.ResultadoPonerBarco.FUERA_DE_TABLERO;
import static hundirlaflota.modelo.ResultadoPonerBarco.NINGUNO;
import static hundirlaflota.modelo.ResultadoPonerBarco.OK;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import tablero.modelo.Agente;

/**
 *
 * @author Ignacio Calvo <ignacio.calvo@gmail.com>
 */
public class ColocadorHumano extends Colocador {

    /**
     *
     * @param jugador
     */
    public ColocadorHumano(String jugador) {
        super(jugador);
    }
        
    @Override
    public Flota nuevaJugada(VistaColocador vista) {
        TableroBarcos tablero = new TableroBarcos(vista.getAltoTablero(), vista.getAnchoTablero());
        Coordenada coord;

        for (Barco barco: vista.getFlota().getBarcos().keySet()) {
            boolean buqueColocado = false;
            do {
                try {
                    IO.mostrarTablero(System.out, tablero);
                    coord = IO.pedirCoordenada("Coordenadas para " + barco.getTipo().getNombre() + " (formato A0)", null);
                    String dirLetra = IO.pedirCadena("Dirección (N, S, E, O)", null) + "";
                    Direccion dir = Direccion.fromAbrev(dirLetra.toUpperCase());
                    ResultadoPonerBarco resultado;
                    resultado = tablero.ponerBarco(barco, coord, dir);
                    
                    switch (resultado) {
                        case NINGUNO:
                            System.err.println("ERROR: datos incorrectos");
                            break;
                        case FUERA_DE_TABLERO:
                            System.err.println("ERROR: se sale del tablero");
                            break;
                        case COLISION:
                            System.err.println("ERROR: colisiona con otro barco");
                            break;
                        case OK:
                            buqueColocado = true;
                            vista.getFlota().getBarcos().put(barco, new ColocacionBarco(coord, dir));
                            break;
                    }
                } catch (IOException ex) {
                    Logger.getLogger(DisparadorHumano.class.getName()).log(Level.SEVERE, null, ex);
                }
            } while(!buqueColocado);
            
        } // for
        return vista.getFlota();
    }

    @Override
    public void notificar(
            Flota jugada, 
            Agente<Flota, VistaColocador> agente,
            VistaColocador vista) {
    }
    
    
}
