/*
 * Copyright (C) 2013 Ignacio Calvo <ignacio.calvo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hundirlaflota.modelo.agentes;

import hundirlaflota.modelo.tablero.Barco;
import hundirlaflota.modelo.ColocacionBarco;
import hundirlaflota.modelo.tablero.Coordenada;
import hundirlaflota.modelo.Direccion;
import hundirlaflota.modelo.Flota;
import hundirlaflota.modelo.ResultadoPonerBarco;
import hundirlaflota.modelo.tablero.TableroBarcos;
import java.util.Random;
import tablero.modelo.Agente;

/**
 *
 * @author Ignacio Calvo <ignacio.calvo@gmail.com>
 */
public class ColocadorSimple extends Colocador {
    
    private ProveedorCoordenadas proveedorCoords;
    
    /**
     *
     * @param jugador
     */
    public ColocadorSimple(String jugador, ProveedorCoordenadas proveedorCoords) {
        super(jugador);
        
        this.proveedorCoords = proveedorCoords;
    }
    
    @Override
    public Flota nuevaJugada(VistaColocador vista) {
        TableroBarcos tablero = new TableroBarcos(vista.getAltoTablero(), vista.getAnchoTablero());
        Random rnd = new Random();
        for (Barco barco: vista.getFlota().getBarcos().keySet()) {
            boolean buqueColocado = false;
            do {
                int x = rnd.nextInt(tablero.getAlto());
                int y = rnd.nextInt(tablero.getAncho());
                int intDir = rnd.nextInt(2);
                Coordenada coord = proveedorCoords.nuevaCoordenada(tablero);
                Direccion dir = Direccion.ESTE;
                if (intDir == 0) {
                    dir = Direccion.SUR;
                }
                ResultadoPonerBarco resultado;
                resultado = tablero.ponerBarco(barco, coord, dir);
                if (resultado == ResultadoPonerBarco.OK) {
                    buqueColocado = true;
                    vista.getFlota().getBarcos().put(barco, new ColocacionBarco(coord, dir));
                }
            } while(!buqueColocado);
        }
        
        return vista.getFlota();
    }

    @Override
    public void notificar(
            Flota jugada, 
            Agente<Flota, VistaColocador> agente,
            VistaColocador vista) {
    }

}
