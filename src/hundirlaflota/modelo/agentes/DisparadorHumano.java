/*
 * Copyright (C) 2013 Ignacio Calvo <ignacio.calvo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hundirlaflota.modelo.agentes;

import hundirlaflota.interfaz.IO;
import hundirlaflota.modelo.tablero.Coordenada;
import hundirlaflota.modelo.tablero.Tablero;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import tablero.modelo.Agente;

/**
 *
 * @author Ignacio Calvo <ignacio.calvo@gmail.com>
 */
public class DisparadorHumano extends Disparador {

    /**
     *
     * @param jugador
     */
    public DisparadorHumano(String jugador) {
        super(jugador);
    }
        
    /**
     * @inheritDoc
     * @param vista
     * @return Jugada escogida por la persona
     */
    @Override
    public Coordenada nuevaJugada(VistaDisparador vista) {
        IO.mostrarTablero(System.out, vista.getTableroEnemigo(), vista.getTableroPropio());
        Coordenada coord = null;
        try {
            coord = IO.pedirCoordenada("Su jugada", null);
        } catch (IOException ex) {
            Logger.getLogger(DisparadorHumano.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return coord;
    }
    
    @Override
    public void notificar(
            Coordenada jugada, 
            Agente<Coordenada, VistaDisparador> agente,
            VistaDisparador vista) {

    }
    
}
