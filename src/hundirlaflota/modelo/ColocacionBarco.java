/*
 * Copyright (C) 2013 Ignacio Calvo <ignacio.calvo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hundirlaflota.modelo;

import hundirlaflota.modelo.tablero.Coordenada;

/**
 * Representa una colocación de un barco en el tablero. Se compone de una
 * coordenada inicial a partir de la cual se dispondrá el barco, y una dirección
 * hacia la cual se extenderá desde la coordenada inicial.
 * @author Ignacio Calvo <ignacio.calvo@gmail.com>
 */
public class ColocacionBarco {
    
    private Coordenada coordenada;
    private Direccion direccion;

    /**
     *
     * @param coordenada
     * @param direccion
     */
    public ColocacionBarco(Coordenada coordenada, Direccion direccion) {
        this.coordenada = coordenada;
        this.direccion = direccion;
    }
    
    /**
     *
     * @return
     */
    public Coordenada getCoordenada() {
        return coordenada;
    }

    /**
     *
     * @return
     */
    public Direccion getDireccion() {
        return direccion;
    }

}
