/*
 * Copyright (C) 2013 Ignacio Calvo <ignacio.calvo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hundirlaflota.modelo;

import hundirlaflota.modelo.tablero.TipoBarco;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Configuraciones globales, entre las que se encuentra las dimensiones
 * de tablero y la composición de la flota. Podrían incorporarse más datos
 * configurables como el tipo de jugadores (humano, IA, diferentes tipos de
 * IA). La clase dispone de una función de carga inicial, que actualmente
 * establece valores "cableados" pero que podría usarse para cargar datos
 * de un fichero o una base de datos.
 * @author Ignacio Calvo <ignacio.calvo@gmail.com>
 */
public class Configuracion {
    private static int altoNuevoTablero;
    private static int anchoNuevoTablero;
    private static Collection<TipoBarco> flota = new ArrayList<>();
    private static boolean depuracion;
    private static long randomSeed;

    public static long getRandomSeed() {
        return randomSeed;
    }

    public static void setRandomSeed(long randomSeed) {
        Configuracion.randomSeed = randomSeed;
    }
    
    /**
     *
     */
    public static void Cargar() {
        altoNuevoTablero = 10;
        anchoNuevoTablero = 10;
        flota.add(TipoBarco.PORTAAVIONES);
        flota.add(TipoBarco.ACORAZADO);
        flota.add(TipoBarco.ACORAZADO);
        flota.add(TipoBarco.FRAGATA);
        flota.add(TipoBarco.FRAGATA);
        flota.add(TipoBarco.SUBMARINO);
        flota.add(TipoBarco.SUBMARINO);
        depuracion = false;
    }

    /**
     *
     * @return
     */
    public static int getAltoNuevoTablero() {
        return altoNuevoTablero;
    }
    
    /**
     *
     * @param filas
     */
    public static void setAltoNuevoTablero(int filas) {
        altoNuevoTablero = filas;
    }

    /**
     *
     * @return
     */
    public static int getAnchoNuevoTablero() {
        return anchoNuevoTablero;
    }

    /**
     *
     * @param columnas
     */
    public static void setAnchoNuevoTablero(int columnas) {
        anchoNuevoTablero = columnas;
    }

    /**
     *
     * @return
     */
    public static Collection<TipoBarco> getFlota() {
        return flota;
    }

    /**
     * Cadena con las dimensiones de los tipos de barco de la flota. Por
     * ejemplo, para la flota estándar es "5,4,4,3,3,2,2".
     * @return Cadena de dimensiones de la flota
     */
    public static String getCadenaFlota() {
        StringBuilder res;
        res = new StringBuilder();
        
        for (TipoBarco elemento: flota) {
            res.append(elemento.getTamanio());
            res.append(",");
        }
        return res.substring(0, res.length()-1);
    }

    /**
     * Setter correspondiente a {@link Configuracion#getCadenaFlota()}.
     * @param cadenaFlota Cadena de dimensiones de la flota
     */
    public static void setCadenaFlota(String cadenaFlota) {
        String[] lista = cadenaFlota.split(",");
        ArrayList<TipoBarco> nuevaFlota = new ArrayList<>();
        for (String elemento: lista) {
            nuevaFlota.add(TipoBarco.fromTamanio(Integer.parseInt(elemento)));
        }
        flota = nuevaFlota;
    }

    public static boolean isDepuracion() {
        return depuracion;
    }

    public static void setDepuracion(boolean depuracion) {
        Configuracion.depuracion = depuracion;
    }

    
}
