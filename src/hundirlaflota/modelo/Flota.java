/*
 * Copyright (C) 2013 Ignacio Calvo <ignacio.calvo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hundirlaflota.modelo;

import hundirlaflota.modelo.tablero.Barco;
import hundirlaflota.modelo.tablero.TipoBarco;
import java.util.Collection;
import java.util.HashMap;


/**
 *
 * @author Ignacio Calvo <ignacio.calvo@gmail.com>
 */
public class Flota {
    private HashMap<Barco, ColocacionBarco> barcos;
    
    /**
     *
     * @param configFlota
     */
    public Flota(Collection<TipoBarco> configFlota) {
        barcos = new HashMap<>();
        for (TipoBarco tipoBarco: configFlota) {
            Barco barco = new Barco(tipoBarco);
            barcos.put(barco, null);
        }
    }

    /**
     *
     * @return
     */
    public HashMap<Barco, ColocacionBarco> getBarcos() {
        return barcos;
    }
 
    
}
