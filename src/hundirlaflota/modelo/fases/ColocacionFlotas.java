/*
 * Copyright (C) 2013 Ignacio Calvo <ignacio.calvo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hundirlaflota.modelo.fases;

import hundirlaflota.interfaz.IO;
import hundirlaflota.modelo.tablero.Barco;
import hundirlaflota.modelo.ColocacionBarco;
import hundirlaflota.modelo.Configuracion;
import hundirlaflota.modelo.Flota;
import hundirlaflota.modelo.JugadorHundirLaFlota;
import hundirlaflota.modelo.ResultadoPonerBarco;
import hundirlaflota.modelo.tablero.TableroBarcos;
import hundirlaflota.modelo.tablero.TipoBarco;
import hundirlaflota.modelo.agentes.VistaColocador;
import java.util.ArrayList;
import java.util.Collection;
import tablero.modelo.Universo;
import tablero.modelo.Agente;
import tablero.modelo.Fase;

/**
 * Fase de colocación de flotas
 * @author Ignacio Calvo <ignacio.calvo@gmail.com>
 */
public class ColocacionFlotas extends Fase<Flota, JugadorHundirLaFlota, VistaColocador, TableroBarcos> {

    /**
     * {@inheritDoc}
     * 
     * @param universo 
     */
    /**
     *
     * @param universo
     */
    public ColocacionFlotas(Universo<JugadorHundirLaFlota, TableroBarcos> universo) {
        super(universo);
    }

    /**
     * Comprueba si el tablero dado es correcto y en tal caso 
     * @param jugada
     * @param agente
     * @return 
     */
    @Override
    public boolean aplicarJugada(Flota jugada, Agente<Flota, VistaColocador> agente) {
        Collection<TipoBarco> flotaJuego = Configuracion.getFlota();
        
        // TODO: comparar flotaJuego y jugada para ver que tienen los mismos tipos de barco
        ArrayList<TipoBarco> tiposJugada = new ArrayList<>();
        
        for (Barco barco: jugada.getBarcos().keySet()) {
            tiposJugada.add(barco.getTipo());
        }
        
        TableroBarcos tablero = getUniverso().getDatosPropios(getJugador(agente));
        
        for (Barco barco: jugada.getBarcos().keySet()) {
            if (jugada.getBarcos().get(barco) == null) {
                return false;
            }
        }
        for (Barco barco: jugada.getBarcos().keySet()) {
            ColocacionBarco colocacion = jugada.getBarcos().get(barco);
            Barco copia = new Barco(barco.getTipo());
            ResultadoPonerBarco resultado;
            resultado = tablero.ponerBarco(copia, colocacion.getCoordenada(), colocacion.getDireccion());
            
            if (resultado != ResultadoPonerBarco.OK) {
                return false;
            }
        }
        
        return true;
    }

    @Override
    public boolean terminada(Agente<Flota, VistaColocador> agente) {
        return getJugador(agente) == getUniverso().getJugador2();
    }

    @Override
    public boolean eliminado(Agente<Flota, VistaColocador> agente) {
        return false;
    }

    @Override
    public String getNombre() {
        return "Fase de colocación";
    }

    @Override
    public Agente<Flota, VistaColocador> getAgente(JugadorHundirLaFlota jugador) {
        return jugador.getColocador();
    }

    @Override
    protected JugadorHundirLaFlota siguienteJugador(JugadorHundirLaFlota jugador) {
        if (jugador == null) {
            return getUniverso().getJugador1();
        }
        else {
            return getUniverso().getEnemigo(jugador);
        }
    }
    
    
    @Override
    protected VistaColocador getVista(JugadorHundirLaFlota jugador) {
        TableroBarcos tab = getUniverso().getDatosPropios(jugador);
        
        return new VistaColocador(tab.getAlto(), tab.getAncho(), new Flota(Configuracion.getFlota()));
    }

    @Override
    protected void depurar(Flota jugada, JugadorHundirLaFlota jugador) {
        if (Configuracion.isDepuracion()) {
            IO.mostrarTablero(System.out, getUniverso().getDatosJugador1(), getUniverso().getDatosJugador2());
        }
    }
    
}
