/*
 * Copyright (C) 2013 Ignacio Calvo <ignacio.calvo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hundirlaflota.modelo.fases;

import hundirlaflota.interfaz.IO;
import hundirlaflota.modelo.Configuracion;
import hundirlaflota.modelo.tablero.Coordenada;
import hundirlaflota.modelo.JugadorHundirLaFlota;
import hundirlaflota.modelo.tablero.TableroBarcos;
import hundirlaflota.modelo.agentes.VistaDisparador;
import hundirlaflota.modelo.tablero.Tablero;
import tablero.modelo.Universo;
import tablero.modelo.Agente;
import tablero.modelo.Fase;

/**
 *
 * @author Ignacio Calvo <ignacio.calvo@gmail.com>
 */
public class Disparos extends Fase<Coordenada,JugadorHundirLaFlota, VistaDisparador, TableroBarcos> {

    /**
     *
     * @param universo
     */
    public Disparos(Universo<JugadorHundirLaFlota, TableroBarcos> universo) {
        super(universo);
    }

    private TableroBarcos getTableroPropio(Agente<Coordenada, VistaDisparador> agente) {
        return getUniverso().getDatosPropios(getJugador(agente));
    }

    private TableroBarcos getTableroEnemigo(Agente<Coordenada, VistaDisparador> agente) {
        return getUniverso().getDatosEnemigo(getJugador(agente));
    }
    
    @Override
    public boolean aplicarJugada(Coordenada jugada, Agente<Coordenada, VistaDisparador> agente) {
        return getTableroEnemigo(agente).disparar(jugada);
    }

    @Override
    public boolean eliminado(Agente<Coordenada, VistaDisparador> agente) {
        return getTableroPropio(agente).vencido();
    }
    
    @Override
    public boolean terminada(Agente<Coordenada, VistaDisparador> agente) {
        return false;
    }

    @Override
    public String getNombre() {
        return "Fase de disparos";
    }
    @Override
    public Agente<Coordenada, VistaDisparador> getAgente(JugadorHundirLaFlota jugador) {
        return jugador.getDisparador();
    }

    @Override
    protected JugadorHundirLaFlota siguienteJugador(JugadorHundirLaFlota jugador) {
        if (jugador == null) {
            return getUniverso().getJugador1();
        }
        else {
            return getUniverso().getEnemigo(jugador);
        }
    }

    @Override
    protected VistaDisparador getVista(JugadorHundirLaFlota jugador) {
        return new VistaDisparador(jugador, getUniverso());
    }

    @Override
    protected void depurar(Coordenada jugada, JugadorHundirLaFlota jugador) {
        Tablero tablero;
        tablero = getUniverso().getDatosEnemigo(jugador);
        System.out.format(
            "%s disparó a %s con resultado de %s\n", 
            jugador.getNombre(),
            jugada.toA0String(),
            tablero.casillaEn(jugada).getTipo().name());
        if (Configuracion.isDepuracion()) {
            IO.mostrarTablero(System.out, getUniverso().getDatosJugador1(), getUniverso().getDatosJugador2());
        }
    }
    
}
