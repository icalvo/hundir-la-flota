/*
 * Copyright (C) 2013 Ignacio Calvo <ignacio.calvo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hundirlaflota.modelo;

import hundirlaflota.modelo.agentes.Colocador;
import hundirlaflota.modelo.agentes.Disparador;
import tablero.modelo.Jugador;

/**
 *
 * @author Ignacio Calvo <ignacio.calvo@gmail.com>
 */
public class JugadorHundirLaFlota implements Jugador {

    private String nombre;
    private Colocador colocador;
    private Disparador disparador;
    private boolean eliminado;

    @Override
    public boolean isEliminado() {
        return eliminado;
    }

    @Override
    public void setEliminado(boolean eliminado) {
        this.eliminado = eliminado;
    }

    /**
     *
     * @param nombre
     */
    public JugadorHundirLaFlota(String nombre) {
        this.nombre = nombre;
    }
   
    
    /**
     *
     * @return
     */
    public Colocador getColocador() {
        return colocador;
    }

    /**
     *
     * @return
     */
    public Disparador getDisparador() {
        return disparador;
    }

    @Override
    public String getNombre() {
        return nombre;
    }

    /**
     *
     * @param colocador
     */
    public void setColocador(Colocador colocador) {
        this.colocador = colocador;
    }

    /**
     *
     * @param disparador
     */
    public void setDisparador(Disparador disparador) {
        this.disparador = disparador;
    }

}
