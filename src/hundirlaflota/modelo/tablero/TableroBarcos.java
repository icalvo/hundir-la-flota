/*
 * Copyright (C) 2013 Ignacio Calvo <ignacio.calvo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hundirlaflota.modelo.tablero;

import hundirlaflota.modelo.Direccion;
import hundirlaflota.modelo.ResultadoPonerBarco;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Ignacio Calvo <ignacio.calvo@gmail.com>
 */
public class TableroBarcos implements Tablero<CasillaBarco> {
    private HashMap<Coordenada, CasillaBarco> mapa;
    private ArrayList<Barco> barcos;
    private final int alto;
    private final int ancho;

    /**
     *
     * @param alto
     * @param ancho
     */
    public TableroBarcos(int alto, int ancho) {
        mapa = new HashMap<>();
        barcos = new ArrayList<>();
        for (int i=0;i<ancho;i++) {
            for (int j=0;j<alto;j++) {
                Coordenada coord = new Coordenada(i, j);
                this.mapa.put(coord, new CasillaBarco());
            }
        }
        this.alto = alto;
        this.ancho = ancho;
    }
    
    /*
     * Sitúa el barco en la posición dada, extendiéndolo hacia la dirección
     * dada.
     */
    /**
     *
     * @param barco
     * @param posicion
     * @param dir
     * @return
     */
    public ResultadoPonerBarco ponerBarco(Barco barco, Coordenada posicion, Direccion dir) {
        if (barco == null) {
            return ResultadoPonerBarco.NINGUNO;
        }
        if (posicion == null) {
            return ResultadoPonerBarco.NINGUNO;
        }
        if (dir == null) {
            return ResultadoPonerBarco.NINGUNO;
        }
        Coordenada actual = posicion;
        ArrayList<CasillaBarco> casillas = new ArrayList<>();

        if (ocupada(actual.atras(dir))) {
            return ResultadoPonerBarco.COLISION;
        }
        for (int i = 1; i <= barco.getTipo().getTamanio(); i++) {
            CasillaBarco casilla = casillaEn(actual);
            if (!esCoordenadaValida(actual)) {
                return ResultadoPonerBarco.FUERA_DE_TABLERO;
            }
            if (casilla.getBarco() != null) {
                return ResultadoPonerBarco.COLISION;
            }
            if (ocupada(actual.izquierda(dir))) {
               return ResultadoPonerBarco.COLISION;
            }
            if (ocupada(actual.derecha(dir))) {
               return ResultadoPonerBarco.COLISION;
            }
            
            casillas.add(casilla);
            actual = actual.adelante(dir);
        }
        if (ocupada(actual)) {
           return ResultadoPonerBarco.COLISION;
        }

        barco.setCasillas(casillas);
        for (CasillaBarco casilla: casillas) {
            casilla.setBarco(barco);
        }
        barcos.add(barco);
        return ResultadoPonerBarco.OK;
    }
    
    boolean ocupada(Coordenada coord) {
        return (esCoordenadaValida(coord) &&
                casillaEn(coord).hasBarco());
    }
    /**
     *
     * @param x
     * @param y
     * @return
     */
    @Override
    public CasillaBarco casillaEn(int x, int y) {
        return mapa.get(new Coordenada(x, y));
    }

    /**
     *
     * @param coord
     * @return
     */
    @Override
    public CasillaBarco casillaEn(Coordenada coord) {
        return mapa.get(coord);
    }
    
    /**
     *
     * @param coord
     * @return
     */
    @Override
    public boolean esCoordenadaValida(Coordenada coord) {
        return mapa.containsKey(coord);
    }

    /**
     * @return the alto
     */
    @Override
    public int getAlto() {
        return alto;
    }

    /**
     * @return the ancho
     */
    @Override
    public int getAncho() {
        return ancho;
    }

    /**
     * Dispara en la coordenada dada.
     * @param jugada Coordenada en la que se dispara.
     * @return ¿Es correcta la jugada? (fuera de rango, o casilla ya disparada)
     */
    public boolean disparar(Coordenada jugada) {
        if (this.esCoordenadaValida(jugada)) {
            return casillaEn(jugada).disparar();
        }
        else {
            return false;
        }
    }

    /**
     *
     * @return
     */
    public boolean vencido() {
        for (Barco barco: barcos) {
            if (!barco.hundido()) {
                return false;
            }
        }
        return true;
    }

    /**
     *
     * @return
     */
    public ArrayList<Barco> getBarcos() {
        return barcos;
    }
    
}
