/*
 * Copyright (C) 2013 Ignacio Calvo <ignacio.calvo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hundirlaflota.modelo.tablero;

/**
 *
 * @author Ignacio Calvo <ignacio.calvo@gmail.com>
 */
public enum TipoBarco {
    /**
     *
     */
    PORTAAVIONES   ("P", "portaaviones", 5),
    /**
     *
     */
    ACORAZADO ("A", "acorazado", 4),
    /**
     *
     */
    FRAGATA    ("F", "fragata", 3),
    /**
     *
     */
    SUBMARINO    ("S", "submarino", 2);
    
    private final String abrev;
    private final String nombre;
    private final int tamanio;

    TipoBarco(String abrev, String nombre, int tamanio) {
        this.abrev = abrev;
        this.nombre = nombre;
        this.tamanio = tamanio;
    }

    /**
     *
     * @param tamanio
     * @return
     */
    public static TipoBarco fromTamanio(int tamanio) {
        for (TipoBarco b : TipoBarco.values()) {
            if (tamanio == b.getTamanio()) {
                return b;
            }
        }
        return null;
    }

    /**
     * @return the abrev
     */
    public String getAbrev() {
        return abrev;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre + " (" + tamanio + " ud.)";
    }

    /**
     * @return the tamanio
     */
    public int getTamanio() {
        return tamanio;
    }
    
}
