/*
 * Copyright (C) 2013 Ignacio Calvo <ignacio.calvo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hundirlaflota.modelo.tablero;

/**
 * Representa una casilla de un tablero. Si un barco ocupa dicha casilla, 
 * y dicho barco es visible por el jugador que maneja el tablero, entonces
 * la casilla tiene una referencia al barco. Las casillas tienen también un 
 * tipo que representa su estado respecto a los disparos (sin disparar,
 * agua, tocado, hundido).
 * @author Ignacio Calvo <ignacio.calvo@gmail.com>
 */
public interface Casilla {

    /**
     * @return Tipo de casilla
     */
    TipoCasilla getTipo();
    
    /**
     * Barco que ocupa la casilla. Será null si la casilla está libre o si
     * el jugador que maneja el tablero no puede ver los barcos.
     * @return Barco que ocupa la casilla.
     */
    Barco getBarco();
}
