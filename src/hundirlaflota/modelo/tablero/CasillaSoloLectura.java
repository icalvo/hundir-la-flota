/*
 * Copyright (C) 2013 Ignacio Calvo <ignacio.calvo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hundirlaflota.modelo.tablero;

/**
 * Casilla con el barco visible, pero de sólo lectura. Estas casillas conforman
 * los tableros de sólo lectura ({@link TableroSoloLectura}), que son objetos
 * que usan los jugadores para ver sus propios tableros.
 * @author Ignacio Calvo <ignacio.calvo@gmail.com>
 */
public class CasillaSoloLectura implements Casilla {

    private Casilla casilla;


    /**
     *
     * @param casilla
     */
    public CasillaSoloLectura(Casilla casilla) {
        this.casilla = casilla;
    }
    
    /**
     * @return the tipo
     */
    @Override
    public TipoCasilla getTipo() {
        return casilla.getTipo();
    }

    /**
     * @return the barco
     */
    @Override
    public Barco getBarco() {
        return casilla.getBarco();
    }

    @Override
    public String toString() {
        return "Casilla{" + "tipo=" + getTipo().getAbrev() + ", barco=" + getBarco() + '}';
    }

}
