/*
 * Copyright (C) 2013 Ignacio Calvo <ignacio.calvo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hundirlaflota.modelo.tablero;

/**
 *
 * @author Ignacio Calvo <ignacio.calvo@gmail.com>
 */
public class TableroOculto implements Tablero<CasillaOculta> {
    private Tablero tablero;

    /**
     *
     * @param tablero
     */
    public TableroOculto(Tablero tablero) {
        this.tablero = tablero;
    }

    /**
     *
     * @param x
     * @param y
     * @return
     */
    @Override
    public CasillaOculta casillaEn(int x, int y) {
        return new CasillaOculta(tablero.casillaEn(x, y));
    }

    /**
     *
     * @param coord
     * @return
     */
    @Override
    public CasillaOculta casillaEn(Coordenada coord) {
        return new CasillaOculta(tablero.casillaEn(coord));
    }

    /**
     *
     * @param coord
     * @return
     */
    @Override
    public boolean esCoordenadaValida(Coordenada coord) {
        return tablero.esCoordenadaValida(coord);
    }

    @Override
    public int getAlto() {
        return tablero.getAlto();
    }

    @Override
    public int getAncho() {
        return tablero.getAncho();
    }
    
    
}
