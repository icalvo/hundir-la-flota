/*
 * Copyright (C) 2013 Ignacio Calvo <ignacio.calvo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hundirlaflota.modelo.tablero;

/**
 *
 * @param <TCasilla> 
 * @author Ignacio Calvo <ignacio.calvo@gmail.com>
 */
public interface Tablero<TCasilla extends Casilla> {

    /**
     *
     * @param x
     * @param y
     * @return
     */
    TCasilla casillaEn(int x, int y);

    /**
     *
     * @param coord
     * @return
     */
    TCasilla casillaEn(Coordenada coord);

    /**
     *
     * @param coord
     * @return
     */
    boolean esCoordenadaValida(Coordenada coord);

    /**
     * @return the alto
     */
    int getAlto();

    /**
     * @return the ancho
     */
    int getAncho();
    
}
