/*
 * Copyright (C) 2013 Ignacio Calvo <ignacio.calvo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hundirlaflota.modelo.tablero;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Representa un barco concreto; por ejemplo, uno de los dos acorazados de
 * algún tablero.
 * @author Ignacio Calvo <ignacio.calvo@gmail.com>
 */
public class Barco {

    private TipoBarco tipo;
    private Collection<CasillaBarco> casillas;

    /**
     * Construye un barco a partir de su tipo. Nótese que el barco se crea
     * sin estar posicionado en ningún lugar.
     * @param tipo Tipo del barco creado
     */
    public Barco(TipoBarco tipo) {
        if (tipo == null) {
            throw new IllegalArgumentException();
        }
        this.tipo = tipo;
        this.casillas = new ArrayList<>();
    }
    
    /**
     *
     * @return
     */
    public Collection<CasillaBarco> getCasillas() {
        return casillas;
    }

    /**
     * Establece la posición del barco, mediante una colección de casillas.
     * Este método hace algunas comprobaciones, pero no comprueba si las 
     * casillas pertenecen todas a un mismo tablero (no podría dado que las
     * casillas no tienen referencia al tablero), ni tampoco si son 
     * consecutivas.
     * @param casillas
     * @throws IllegalArgumentException Si se pasa un objeto nulo
     * @throws IllegalArgumentException Si la colección no tiene un tamaño 
     * igual al tamaño del tipo de barco
     * @throws IllegalArgumentException Si el barco ya está posicionado
     */
    void setCasillas(Collection<CasillaBarco> casillas) throws IllegalArgumentException {
        if (casillas == null)
            throw new IllegalArgumentException("Colección de casillas nula");
        if (casillas.size() != this.getTipo().getTamanio())
            throw new IllegalArgumentException("Colección de casillas de tamaño incorrecto");
        if (!getCasillas().isEmpty()) {
            throw new IllegalArgumentException("Este barco ya está posicionado");
        }
        getCasillas().addAll(casillas);
    }

    /**
     *
     * @return
     */
    public TipoBarco getTipo() {
        return tipo;
    }

    /**
     *
     * @param tipo
     */
    public void setTipo(TipoBarco tipo) {
        if (tipo == null) {
            throw new IllegalArgumentException();
        }
        this.tipo = tipo;
    }

    /**
     * Determina si el barco está hundido. Nótese que un barco sin posicionar
     * no puede estar hundido.
     * @return ¿Está hundido el barco?
     */
    public boolean hundido() {
        // Si la primera casilla está HUNDIDO
        for (CasillaBarco casillaBarco: getCasillas()) {
            return (casillaBarco.getTipo() == TipoCasilla.HUNDIDO);
        }        
        return false;
    }
    
    @Override
    public String toString() {
        return "Barco{" + "tipo=" + tipo + ", casillas=" + casillas + '}';
    }

    
    
}
