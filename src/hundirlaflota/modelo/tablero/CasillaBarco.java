/*
 * Copyright (C) 2013 Ignacio Calvo <ignacio.calvo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hundirlaflota.modelo.tablero;

/**
 * Casilla con el barco visible. Estas casillas conforman los tableros
 * visibles ({@link TableroBarcos}), que son objetos que conforman el universo
 * de juego y que usan las fases para aplicar las jugadas.
 * @author Ignacio Calvo <ignacio.calvo@gmail.com>
 */
public class CasillaBarco implements Casilla {
    private TipoCasilla tipo;
    private Barco barco;
    
    CasillaBarco() {
       this.tipo = TipoCasilla.SIN_DISPARAR;
       this.barco = null;
    }

    /**
     * @return the tipo
     */
    @Override
    public TipoCasilla getTipo() {
        return tipo;
    }

    private void setTipo(TipoCasilla tipo) {
        this.tipo = tipo;
    }

    /**
     * @return the barco
     */
    @Override
    public Barco getBarco() {
        return barco;
    }

    /**
     * @param barco the barco to set
     */
    public void setBarco(Barco barco) {
        this.barco = barco;
    }

    @Override
    public String toString() {
        return "Casilla{" + "tipo=" + tipo.getAbrev() + ", barco=" + barco + '}';
    }

    boolean disparar() {
        // Disparar a una casilla ya disparada es inválido (devolvemos false)
        if (getTipo() != TipoCasilla.SIN_DISPARAR) {
            System.err.println("La casilla ya ha sido disparada");
            return false;
        }

        // Si no hay barco, AGUA
        if (getBarco() == null) {
            setTipo(TipoCasilla.AGUA);
            return true;
        }
        
        // Marcamos temporalmente como TOCADO
        setTipo(TipoCasilla.TOCADO);
        
        // Si alguna casilla del barco está SIN_DISPARAR, volvemos
        for (CasillaBarco casillaBarco: getBarco().getCasillas()) {
            if (casillaBarco.getTipo() == TipoCasilla.SIN_DISPARAR) {
                return true;
            }
            assert casillaBarco.getTipo() == TipoCasilla.TOCADO;
        }
        
        // Si todas están TOCADO, ponemos todas las casillas a HUNDIDO
        for (CasillaBarco casillaBarco: getBarco().getCasillas()) {
            casillaBarco.setTipo(TipoCasilla.HUNDIDO);
        }
        return true;
    }

    boolean hasBarco() {
        return (getBarco() != null);
    }

}
