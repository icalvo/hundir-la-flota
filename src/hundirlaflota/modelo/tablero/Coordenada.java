/*
 * Copyright (C) 2013 Ignacio Calvo <ignacio.calvo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hundirlaflota.modelo.tablero;

import hundirlaflota.modelo.Direccion;
import static hundirlaflota.modelo.Direccion.ESTE;
import static hundirlaflota.modelo.Direccion.NORTE;
import static hundirlaflota.modelo.Direccion.OESTE;
import static hundirlaflota.modelo.Direccion.SUR;

/**
 * Coordenada cartesiana entera de 2 dimensiones. Representa además una jugada
 * de la fase de disparos.
 * @author Ignacio Calvo <ignacio.calvo@gmail.com>
 */
public class Coordenada {
    private int x;
    private int y;
    
    /**
     * Construye una coordenada a partir de sus coordenadas cartesianas
     * @param x
     * @param y
     */
    public Coordenada(int x, int y) {
        this.x = x;
        this.y = y;
    }
    
    /**
     * Construye una coordenada a partir del código A0 (letra y número)
     * @param codigo 
     */
    public Coordenada(String codigo) {
        if (codigo.length() != 2) {
            throw new IllegalArgumentException();
        }
        char letraFila = codigo.toUpperCase().charAt(0);
        String numeroCol = codigo.substring(1, 2);
        
        char letraA = 'A';
        
        this.x = letraFila - letraA;
        this.y = Integer.parseInt(numeroCol);
    }

    /**
     * Abscisas
     * @return
     */
    public int getX() {
        return this.x;
    }

    /**
     * Ordenada
     * @return
     */
    public int getY() {
        return this.y;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Coordenada other = (Coordenada) obj;
        if (this.x != other.x) {
            return false;
        }
        if (this.y != other.y) {
            return false;
        }
        return true;
    }
    
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + this.x;
        hash = 97 * hash + this.y;
        return hash;
    }
    
    @Override
    public String toString() {
        return toA0String();
        //return String.format("[%d,%d]", x, y);
    }

    /**
     * Cadena en formato A0 (letra y número)
     * @return 
     */
    public String toA0String() {
        char letra = (char)('A' + this.x);
        return letra + "" + this.y;
    }

    /**
     * Coordenada que se obtiene avanzando una casilla en la dirección dada,
     * desde la actual
     * @param dir Dirección de avance
     * @return 
     */
    public Coordenada adelante(Direccion dir) {
        switch (dir) {
            case ESTE:
                return new Coordenada(getX(), getY() + 1);
            case SUR:
                return new Coordenada(getX() + 1, getY());
            case NORTE:
                return new Coordenada(getX() - 1, getY());
            case OESTE:
                return new Coordenada(getX(), getY() - 1);
        }
        throw new IllegalArgumentException();
    }
    
    /**
     * Coordenada que se obtiene retrocediendo una casilla en la dirección dada,
     * desde la actual
     * @param dir
     * @return 
     */
    public Coordenada atras(Direccion dir) {
        switch (dir) {
            case ESTE:
                return new Coordenada(getX(), getY() - 1);
            case SUR:
                return new Coordenada(getX() - 1, getY());
            case NORTE:
                return new Coordenada(getX() + 1, getY());
            case OESTE:
                return new Coordenada(getX(), getY() + 1);
        }
        throw new IllegalArgumentException();
    }    

    /**
     * Coordenada que se obtiene yendo a la derecha una casilla respecto a la
     * dirección dada, desde la actual
     * @param dir
     * @return 
     */
    public Coordenada derecha(Direccion dir) {
        switch (dir) {
            case ESTE:
                return new Coordenada(getX() + 1, getY());
            case SUR:
                return new Coordenada(getX(), getY() - 1);
            case NORTE:
                return new Coordenada(getX(), getY() + 1);
            case OESTE:
                return new Coordenada(getX() - 1, getY());
        }
        throw new IllegalArgumentException();
    }    

    /**
     * Coordenada que se obtiene yendo a la izquierda una casilla respecto a la
     * dirección dada, desde la actual
     * @param dir
     * @return 
     */
    public Coordenada izquierda(Direccion dir) {
        switch (dir) {
            case ESTE:
                return new Coordenada(getX() - 1, getY());
            case SUR:
                return new Coordenada(getX(), getY() + 1);
            case NORTE:
                return new Coordenada(getX(), getY() - 1);
            case OESTE:
                return new Coordenada(getX() + 1, getY());
        }
        throw new IllegalArgumentException();
    }

    /**
     * Coordenada al norte de la actual
     * @return 
     */
    public Coordenada norte() {
        return adelante(NORTE);
    }
    
    /**
     * Coordenada al sur de la actual
     * @return 
     */
    public Coordenada sur() {
        return adelante(SUR);
    }

    /**
     * Coordenada al este de la actual
     * @return 
     */
    public Coordenada este() {
        return adelante(ESTE);
    }

    /**
     * Coordenada al oeste de la actual
     * @return 
     */
    public Coordenada oeste() {
        return adelante(OESTE);
    }
    
}
