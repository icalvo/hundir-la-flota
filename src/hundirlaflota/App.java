/*
 * Copyright (C) 2013 Ignacio Calvo <ignacio.calvo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hundirlaflota;

import hundirlaflota.interfaz.menus.Menu;
import hundirlaflota.interfaz.acciones.AccionMenu;
import hundirlaflota.interfaz.menus.Principal;
import hundirlaflota.modelo.Configuracion;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Aplicación de consola para el juego "Hundir la flota"
 * @author Ignacio Calvo <ignacio.calvo@gmail.com>
 */
public class App {

    /**
     * @param args Argumentos de línea de comandos
     */
    public static void main(String[] args) {
        Configuracion.Cargar();
        
        Menu menu = new Principal();
        for (;;) {
            AccionMenu acción;
            try {
                acción = menu.elegirAcción();
                acción.ejecutar();
                System.out.println();
                menu = acción.destino(menu);
            } catch (IOException ex) {
                Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
                System.err.println("ERROR: Ocurrió una excepción de E/S");
            }
        }
    }
}
