/*
 * Copyright (C) 2013 Ignacio Calvo <ignacio.calvo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hundirlaflota.interfaz.acciones;

import hundirlaflota.interfaz.IO;
import hundirlaflota.interfaz.menus.Menu;
import hundirlaflota.modelo.Configuracion;
import hundirlaflota.modelo.JuegoHundirLaFlotaAuto;
import java.io.IOException;

/**
 * Acción de menú para jugar una partida de Hundir la flota.
 * @author Ignacio Calvo <ignacio.calvo@gmail.com>
 */
public class Demo extends AccionMenu {

    /**
     * Ejecuta una partida completa
     */
    @Override
    public void ejecutar() throws IOException {
        int semilla = IO.pedirEntero("Semilla", 1);
        
        Configuracion.setRandomSeed(semilla);
        JuegoHundirLaFlotaAuto juego = new JuegoHundirLaFlotaAuto();
        
        boolean dep = Configuracion.isDepuracion();
        
        Configuracion.setDepuracion(true);
        juego.jugar();
        Configuracion.setDepuracion(dep);
    }
    
    @Override
    public Menu destino(Menu menuOrigen) {
        return menuOrigen;
    }

    @Override
    public String texto() {
        return "Demostración";
    }
    
}
