/*
 * Copyright (C) 2013 Ignacio Calvo <ignacio.calvo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hundirlaflota.interfaz.acciones;

import hundirlaflota.interfaz.menus.Menu;
import java.io.IOException;

/**
 * Acción de menú
 * @author Ignacio Calvo <ignacio.calvo@gmail.com>
 */
public abstract class AccionMenu {
        
    /**
     * Ejecuta la acción de menú
     * @throws IOException 
     */
    public abstract void ejecutar() throws IOException;
    
    /**
     * Menú al que vamos después de ejecutar la acción.
     * @param menuOrigen Menú del que venimos
     * @return Menú al que vamos
     */
    public Menu destino(Menu menuOrigen) {
        return menuOrigen;
    }
    
    /**
     * Nombre de la acción
     * @return Nombre de la acción
     */
    public abstract String texto();
}

