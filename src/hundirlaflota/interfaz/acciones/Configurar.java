/*
 * Copyright (C) 2013 Ignacio Calvo <ignacio.calvo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hundirlaflota.interfaz.acciones;

import hundirlaflota.interfaz.IO;
import hundirlaflota.modelo.Configuracion;
import java.io.IOException;

/**
 * Acción de menú dedicada a la configuración del juego.
 * 
 * @author Ignacio Calvo <ignacio.calvo@gmail.com>
 */
public class Configurar extends AccionMenu {
    
    @Override
    public void ejecutar() throws IOException {
        int filas = IO.pedirEntero("Número de filas del tablero", Configuracion.getAltoNuevoTablero());
        int columnas = IO.pedirEntero("Número de columnas del tablero", Configuracion.getAnchoNuevoTablero());
        String flota = IO.pedirCadena("Flota (lista separada por comas de tamaños de barco)", Configuracion.getCadenaFlota());

        Configuracion.setAltoNuevoTablero(filas);
        Configuracion.setAnchoNuevoTablero(columnas);
        Configuracion.setCadenaFlota(flota);
    }

    @Override
    public String texto() {
        return "Configurar";
    }
    
    
}
