/*
 * Copyright (C) 2013 Ignacio Calvo <ignacio.calvo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hundirlaflota.interfaz.menus;

import hundirlaflota.interfaz.acciones.AccionMenu;
import hundirlaflota.interfaz.acciones.Salir;
import hundirlaflota.interfaz.acciones.Configurar;
import hundirlaflota.interfaz.acciones.Demo;
import hundirlaflota.interfaz.acciones.Jugar;

/**
 * Menú principal
 * @author Ignacio Calvo <ignacio.calvo@gmail.com>
 */
public class Principal extends Menu {
    
    @Override
    public String getNombre() {
        return "PRINCIPAL";
    }

    @Override
    public AccionMenu[] getAcciones() {        
        AccionMenu[] resultado = { 
            new Jugar(),
            new Configurar(),
            new Demo(),
            new Salir()
        };
        return resultado;
    }
}
