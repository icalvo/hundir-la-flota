/*
 * Copyright (C) 2013 Ignacio Calvo <ignacio.calvo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hundirlaflota.interfaz.menus;

import hundirlaflota.interfaz.IO;
import hundirlaflota.interfaz.acciones.AccionMenu;
import java.io.IOException;

/**
 * Representa un menú de opciones
 * @author Ignacio Calvo <ignacio.calvo@gmail.com>
 */
public abstract class Menu {

    /**
     * Nombre del menú
     * @return Nombre del menú
     */
    public abstract String getNombre();
    
    /**
     * Acciones de las que se compone el menú
     * @return Array de acciones de menú
     */
    public abstract AccionMenu[] getAcciones();
    
    /**
     * Muestra el menú y da a elegir una opción.
     * @return La acción escogida
     * @throws IOException 
     */
    public AccionMenu elegirAcción() throws IOException {
        System.out.format("MENÚ %s\n", getNombre().toUpperCase());
        int i = 1;
        for (AccionMenu itMenu: getAcciones()) {
            System.out.format("%d. %s\n", i, itMenu.texto());
            i++;
        }
        for(;;) {
            prompt();
            int respuesta = IO.pedirEntero("Escoja una opción", getAcciones().length);
            if (respuesta > 0 && respuesta <= getAcciones().length) {
                return getAcciones()[respuesta - 1];
            }
            else {
                System.err.println("Opción incorrecta");
            }
        }
    }
    
    /**
     * Imprime un prompt. Puede contener una breve información de contexto.
     */
    private static void prompt() {
        System.out.print("> ");
    }
}
