/*
 * Copyright (C) 2013 Ignacio Calvo <ignacio.calvo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hundirlaflota.interfaz;

import hundirlaflota.modelo.tablero.Casilla;
import hundirlaflota.modelo.Configuracion;
import hundirlaflota.modelo.tablero.Coordenada;
import hundirlaflota.modelo.tablero.Tablero;
import hundirlaflota.modelo.tablero.TipoCasilla;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;

/**
 * Clase encargada de la entrada y salida interactiva de datos.
 * @author Ignacio Calvo <ignacio.calvo@gmail.com>
 */
public class IO {


    /**
     * Pide una cadena de caracteres a la entrada estándar
     * @param msj
     * @param valorDefecto
     * @return
     * @throws IOException
     */
    public static String pedirCadena(String msj, String valorDefecto) throws IOException {
        if (!msj.isEmpty()) {
            System.out.print(msj);
        }
        if (valorDefecto == null) {
            System.out.print(": ");
        }
        else {
            System.out.format(" [%s]: ", valorDefecto);
        }
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));      

        String resultado = reader.readLine();
        if (resultado.isEmpty()) {
            return valorDefecto;
        }
        return resultado;
    }

    
    /**
     *
     * @param msj
     * @param valorDefecto
     * @return
     * @throws IOException
     */
    public static int pedirEntero(String msj, int valorDefecto) throws IOException {
        String lectura = pedirCadena(msj, Integer.toString(valorDefecto));
        int resultado = valorDefecto;
        try {
            resultado = Integer.parseInt(lectura);
        }
        catch (NumberFormatException ex) {
            System.err.format("ERROR: Número inválido (se asume %d)", valorDefecto);
        }
        
        return resultado;        
    }

    /**
     *
     * @param msj
     * @param valorDefecto
     * @return
     * @throws IOException
     */
    public static Coordenada pedirCoordenada(String msj, Coordenada valorDefecto) throws IOException {

        String nombreCoordenadaDefecto = null;
        if (valorDefecto != null) {
            nombreCoordenadaDefecto = valorDefecto.toA0String();
        }
        
        String cadenaCoord = pedirCadena(msj, nombreCoordenadaDefecto);

        if (cadenaCoord == null) {
            if (nombreCoordenadaDefecto == null) {
                return valorDefecto;
            }
        }
        else {
            if (cadenaCoord.equals(nombreCoordenadaDefecto)) {
                return valorDefecto;
            }
        }
        Coordenada resultado;
        try {
            resultado = new Coordenada(cadenaCoord);
            if (Configuracion.getAltoNuevoTablero() < resultado.getX()) {
                System.err.format("ERROR: Fuera de rango");
                return null;
            }
            if (Configuracion.getAnchoNuevoTablero() < resultado.getY()) {
                System.err.format("ERROR: Fuera de rango");
                return null;
            }
        }
        catch(IllegalArgumentException ex) {
            System.err.format("ERROR: Cadena incorrecta para coordenada");
            return null;
        }
        return resultado;
    } 

    /**
     *
     * @param output
     * @param tablero
     */
    public static void mostrarTablero(PrintStream output, Tablero tablero) {
        output.print("  ");
        for (int j = 0; j < tablero.getAncho(); j++) {
            output.print(j);
            output.print(' ');
        }
        output.println();
        char letra = 'A';
        for (int i = 0; i < tablero.getAlto(); i++) {
            output.print(letra + " ");
            for (int j = 0; j < tablero.getAncho(); j++) {
                Casilla casilla = tablero.casillaEn(i, j);
                
                if (casilla.getTipo() == TipoCasilla.SIN_DISPARAR && 
                    casilla.getBarco() != null) {
                    output.print(casilla.getBarco().getTipo().getAbrev() + " ");
                }
                else {
                    output.print(casilla.getTipo().getAbrev() + " ");
                }                    
            }
            output.println();
            letra = (char) (letra + 1);
        }
    }


    /**
     *
     * @param output
     * @param tablero1
     * @param tablero2 
     */
    public static void mostrarTablero(PrintStream output, Tablero tablero1, Tablero tablero2) {
        output.print("  ");
        for (int j = 0; j < tablero1.getAncho(); j++) {
            output.print(j);
            output.print(' ');
        }

        output.print("   ");
        for (int j = 0; j < tablero1.getAncho(); j++) {
            output.print(j);
            output.print(' ');
        }

        output.println();
        char letra = 'A';
        for (int i = 0; i < tablero1.getAlto(); i++) {
            output.print(letra + " ");
            for (int j = 0; j < tablero1.getAncho(); j++) {
                Casilla casilla = tablero1.casillaEn(i, j);
                
                if (casilla.getTipo() == TipoCasilla.SIN_DISPARAR && 
                    casilla.getBarco() != null) {
                    output.print(casilla.getBarco().getTipo().getAbrev() + " ");
                }
                else {
                    output.print(casilla.getTipo().getAbrev() + " ");
                }                    
            }
            
            output.print(" " + letra + " ");
            for (int j = 0; j < tablero2.getAncho(); j++) {
                Casilla casilla = tablero2.casillaEn(i, j);
                
                if (casilla.getTipo() == TipoCasilla.SIN_DISPARAR && 
                    casilla.getBarco() != null) {
                    output.print(casilla.getBarco().getTipo().getAbrev() + " ");
                }
                else {
                    output.print(casilla.getTipo().getAbrev() + " ");
                }                    
            }            
            output.println();
            letra = (char) (letra + 1);
        }
    }
    
}
