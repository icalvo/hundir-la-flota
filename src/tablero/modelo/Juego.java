/*
 * Copyright (C) 2013 Ignacio Calvo <ignacio.calvo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package tablero.modelo;

import java.util.Collection;

/**
 * Juego por turnos genérico.
 * @param <TJugador> Tipo de jugador
 * @param <TDatosJugador> Tipo de los datos asociados a cada jugador
 * @author Ignacio Calvo <ignacio.calvo@gmail.com>
 */
public abstract class Juego<TJugador extends Jugador, TDatosJugador> {

    /**
     * Obtiene el universo de juego
     * @return 
     */
    protected abstract Universo<TJugador, TDatosJugador> getUniverso();

    /**
     * Método principal
     * @return Devuelve una colección con los jugadores vencedores, que
     * puede estar vacía.
     */
    public Collection<TJugador> jugar() {
        Universo<TJugador, TDatosJugador> universo;
        
        universo = getUniverso();
        Collection<TJugador> jugadores = universo.getJugadores();
        
        Collection<Fase> fases = this.getFases(universo);

        for (Fase fase: fases) {
            ResultadoFase resultado;
            System.out.println(fase.getNombre());
            resultado = fase.jugar();
            if (resultado == ResultadoFase.JUEGO_TERMINADO) {
                break;
            }
        } // for Fases
        
        return jugadores;       
    }

    /**
     * Crea las fases del juego
     * @return 
     */
    protected abstract Collection<Fase> getFases(Universo<TJugador, TDatosJugador> universo);
    
}
