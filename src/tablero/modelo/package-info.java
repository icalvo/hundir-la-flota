/*
 * Copyright (C) 2013 Ignacio Calvo <ignacio.calvo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Este paquete contiene un marco de trabajo general para juegos de tablero por
 * turnos.
 * <p>
 * Un juego de tablero se inicia mediante un derivado de la clase {@link Juego},
 * el cual maneja un {@link Universo} y unos jugadores. El juego se divide en
 * varias fases, para cada una de las cuales cada jugador proporciona un
 * agente que sabe elaborar jugadas en dicha fase. 
 * 
 * En la fase se determina el turno de jugadores, se pregunta a cada uno
 * su jugada (también puede abandonar), se intenta ejecutar la jugada y en
 * caso de éxito se determinan los jugadores eliminados. La fase puede 
 * terminar por sí misma o bien porque el juego acabe (cuando sólo quede 1
 * jugador o ninguno por eliminar).
 */
package tablero.modelo;
