/*
 * Copyright (C) 2013 Ignacio Calvo <ignacio.calvo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package tablero.modelo;

import java.util.ArrayList;
import java.util.Collection;

/**
 * En general, en un universo tenemos una lista de jugadores y otra lista de
 * modelos asociados a cada jugador. En estos modelos puede haber elementos
 * compartidos, pero en general debemos asociar una instancia a cada jugador.
 * 
 * @param <TDatos> Tipo de los datos asociados a cada jugador
 * @param <TJugador> Tipo de los jugadores
 * @author Ignacio Calvo <ignacio.calvo@gmail.com>
 */
public class Universo<TJugador, TDatos> {

    /**
     * Lista de jugadores
     */
    private ArrayList<TJugador> jugadores;

    /**
     * Lista de datos de cada jugador
     */
    private ArrayList<TDatos> datosJugador;

    /**
     * Construye un universo a partir de los jugadores y de sus datos.
     * 
     * @param jugadores
     * @param datosJugador 
     * 
     * @throws IllegalArgumentException jugadores es null, datosJugador es null
     * o el tamaño de jugadores y datosJugador difiere
     */
    public Universo(Collection<TJugador> jugadores, Collection<TDatos> datosJugador) {
        if (jugadores == null) {
            throw new IllegalArgumentException();
        }
        if (datosJugador == null) {
            throw new IllegalArgumentException();
        }
        if (jugadores.size() == datosJugador.size()) {
            throw new IllegalArgumentException();
        }

        this.jugadores = new ArrayList<>(jugadores);
        this.datosJugador = new ArrayList<>(datosJugador);
    }
    
    /**
     * Constructor para casos de tipo ajedrez, es decir, con dos jugadores y
     * un solo objeto de datos, que es el tablero, común a ambos.
     * @param jugador1
     * @param jugador2
     * @param tablero 
     */
    public Universo(TJugador jugador1, TJugador jugador2, TDatos tablero) {
        this.jugadores = new ArrayList<>();
        this.datosJugador = new ArrayList<>();
        jugadores.add(jugador1);
        jugadores.add(jugador2);
        datosJugador.add(tablero);
        datosJugador.add(tablero);
    }
    
    /**
     * Constructor para casos de tipo Hundir la flota: dos jugadores y sendos
     * tableros "privados". El tablero de un jugador sólo es conocido
     * parcialmente por el otro jugador.
     * @param jugador1
     * @param tablero1
     * @param jugador2
     * @param tablero2 
     */
    public Universo(TJugador jugador1, TDatos tablero1, TJugador jugador2, TDatos tablero2) {
        this.jugadores = new ArrayList<>();
        this.datosJugador = new ArrayList<>();
        jugadores.add(jugador1);
        jugadores.add(jugador2);
        datosJugador.add(tablero1);
        datosJugador.add(tablero2);
    }

    /**
     * 
     * @return Colección de jugadores
     */
    public Collection<TJugador> getJugadores() {
        return jugadores;
    }

    /**
     * 
     * @return Primer jugador
     */
    public TJugador getJugador1() {
        return jugadores.get(0);
    }

    /**
     * 
     * @return Segundo jugador
     */
    public TJugador getJugador2() {
        return jugadores.get(1);
    }

    /**
     * 
     * @return Datos del primer jugador
     */
    public TDatos getDatosJugador1() {
        return datosJugador.get(0);
    }

    /**
     * 
     * @return Datos del segundo jugador
     */
    public TDatos getDatosJugador2() {
        return datosJugador.get(1);
    }


    /**
     * Datos del jugador enemigo
     * @param jugador
     * @return 
     * 
     * Si el jugador es el 1, devuelve el jugador 2.
     * Si el jugador es el 2, devuelve el jugador 1.
     * En cualquier otro caso, devuelve null.
     */
    public TJugador getEnemigo(TJugador jugador) {
        if (jugador == getJugador1()) {
            return getJugador2();
        }
        else if (jugador == getJugador2()) {
            return getJugador1();
        }
        return null;
    }
    
    /**
     * 
     * @param jugador Jugador del que se quieren los datos propios
     * @return 
     */
    public TDatos getDatosPropios(TJugador jugador) {
        return datosJugador.get(jugadores.indexOf(jugador));
    }

    /**
     * Datos del jugador enemigo
     * @param jugador
     * @return 
     * 
     * Si el jugador es el 1, devuelve los del jugador 2.
     * Si el jugador es el 2, devuelve los del jugador 1.
     * En cualquier otro caso, devuelve null.
     */
    public TDatos getDatosEnemigo(TJugador jugador) {
        if (jugador == getJugador1()) {
            return getDatosJugador2();
        }
        else if (jugador == getJugador2()) {
            return getDatosJugador1();
        }
        return null;
    }

    /**
     * 
     * @param jugador
     * @return Todos los datos de jugador salvo los del dado
     */
    public Collection<TDatos> getDatosEnemigos(Jugador jugador) {
        ArrayList<TDatos> resultado = new ArrayList<>(datosJugador);
        resultado.remove(jugadores.indexOf(jugador));
        return resultado;
    }
    
}
