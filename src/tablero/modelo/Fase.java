/*
 * Copyright (C) 2013 Ignacio Calvo <ignacio.calvo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package tablero.modelo;

import java.util.ArrayList;

/**
 * @param <TJugada> Tipo de la jugada que los jugadores elaboran en su turno
 * @param <TJugador> Tipo de los jugadores
 * @param <TVista> Tipo de la vista que los jugadores tienen del universo en 
 * esta fase
 * @param <TDatos> Tipo de los datos asociados a cada juego
 * @author Ignacio Calvo <ignacio.calvo@gmail.com>
 */
public abstract class Fase<
        TJugada, 
        TJugador extends Jugador, 
        TVista, 
        TDatos> {
    
    /**
     * 
     */
    private Universo<TJugador, TDatos> universo;

    private int cuentaJugadas = 0;
    
    /**
     * 
     * @param universo 
     */
    protected Fase(Universo<TJugador, TDatos> universo) {
        this.universo = universo;
    }

    /**
     * 
     * @return 
     */
    public Universo<TJugador, TDatos> getUniverso() {
        return universo;
    }
    
    /**
     * Nombre de la fase
     * @return Nombre de la fase
     */
    public abstract String getNombre();

    /**
     * Agente de un jugador dado para esta fase
     * @param jugador Jugador del que se quiere obtener su agente para esta fase
     * @return Agente de un jugadorTurno dado para esta fase
     */
    public abstract Agente<TJugada, TVista> getAgente(TJugador jugador);
    

    /**
     * Jugador de un agente dado
     * @param agente Agente del que se quiere obtener su jugador
     * @return Jugador de un agente dado
     */
    protected TJugador getJugador(Agente<TJugada, TVista> agente) {
        return jugadores.get(agentes.indexOf(agente));
    }
        
    /**
     * Aplica la jugada, y devuelve false si la jugada es incorrecta.
     * @param jugada Jugada que se aplicará
     * @param agente Agente que generó la jugada
     * @return ¿Ha sido una jugada correcta?
     */
    public abstract boolean aplicarJugada(TJugada jugada, Agente<TJugada, TVista> agente);

    /**
     * La fase puede terminar por dos motivos: la eliminación del resto de
     * jugadores, u otro motivo distinto. Esta función corresponde a ese
     * otro motivo, y se evalúa una vez por agente (tras jugarTurno el agente). Si
     * la fase sólo termina con un ganador o con tablas, debe devolver false.
     * 
     * @param agente 
     * @return ¿Ha terminado la fase, sin contar eliminación de jugadores?
     */
    public abstract boolean terminada(Agente<TJugada, TVista> agente);

    /**
     * ¿Ha sido eliminado el agente?
     * @param agente
     * @return ¿Ha sido eliminado el agente?
     */
    public abstract boolean eliminado(Agente<TJugada, TVista> agente);

    /**
     * Lista de jugadores activos
     */
    private ArrayList<TJugador> jugadores;
    
    /**
     * Lista de jugadores activos
     */
    private ArrayList<Agente<TJugada, TVista>> agentes;
    
    /**
     * Juega esta fase
     * @return Un resultado de fase que puede ser: FASE_TERMINADA
     * o JUEGO_TERMINADO.
     */
    public ResultadoFase jugar() {
        // Creamos los agentes propios de la fase
        agentes = new ArrayList<>();
        jugadores = new ArrayList<>(getUniverso().getJugadores());
        int eliminados = 0;
        
        for (TJugador jugador: jugadores) {
            agentes.add(getAgente(jugador));
            if (jugador.isEliminado()) {
                eliminados += 1;
            }
        }
        
        TJugador jugadorTurno = null;

        for(;;) {
            jugadorTurno = siguienteJugador(jugadorTurno);
            
            Agente<TJugada, TVista> agenteTurno = getAgente(jugadorTurno);

            cuentaJugadas += 1;
            System.out.format("%d. Turno de %s\n", cuentaJugadas, jugadorTurno.getNombre());
            TVista vista = getVista(jugadorTurno);
            TJugada jugada = agenteTurno.nuevaJugada(vista);
            if (jugada == null) {
               // Abandono
               System.out.format("Abandona %s\n", jugadorTurno.getNombre());
               jugadorTurno.setEliminado(true);
               eliminados += 1;
            }
            else {
               boolean esJugadaCorrecta;
               esJugadaCorrecta = aplicarJugada(jugada, agenteTurno);

               if (esJugadaCorrecta) {
                   for (Agente<TJugada, TVista> agente: agentes) {
                       if (eliminado(agente)) {
                           getJugador(agente).setEliminado(true);
                           eliminados += 1;
                       }
                       agente.notificar(jugada, agenteTurno, getVista(getJugador(agente)));
                   }
                   depurar(jugada, jugadorTurno);
               }
               else {
                   System.out.format("Jugada incorrecta de %s (%s)\n", jugadorTurno.getNombre(), jugada);
               }
            }

            int noEliminados = jugadores.size() - eliminados;
            if (noEliminados <= 1) {
               // TABLAS O GANADOR
                if (noEliminados == 1) {
                    for (TJugador jugador2: jugadores) {
                        if (!jugador2.isEliminado()) {
                            System.out.format("Ganador %s\n", jugador2.getNombre());
                            break;
                        }
                    }

                }
                else {
                    System.out.println("Tablas");
                }
               return ResultadoFase.JUEGO_TERMINADO;
            }
            if (terminada(agenteTurno)) {
               System.out.println(getNombre() + " terminada");
               return ResultadoFase.FASE_TERMINADA;
            }
        } // for
    } // jugar()
    
    /**
     * Implementación estándar de siguienteJugador: da el turno al siguiente
     * jugadorTurno no eliminado, considerando circular la lista de jugadores.
     * @param jugador 
     * @return Jugador (no eliminado) siguiente al dado.
     */
    protected TJugador siguienteJugador(TJugador jugador) {
        int index = jugadores.indexOf(jugador);
        TJugador jugadorSiguiente;
        do {
            index += 1;
            if (index == jugadores.size()) {
                index = 0;
            }
            jugadorSiguiente = jugadores.get(index);
        } while (!jugadorSiguiente.isEliminado());

        return jugadorSiguiente;
    }
    
    /**
     * Obtiene la vista para un determinado jugador
     * @param jugador Jugador del que se quiere la vista
     * @return Vista para el jugador
     */
    protected abstract TVista getVista(TJugador jugador);
        
    /**
     * 
     * @param jugada
     * @param agente
     * @param vista 
     */
    protected abstract void depurar(
            TJugada jugada, 
            TJugador jugador);
}
