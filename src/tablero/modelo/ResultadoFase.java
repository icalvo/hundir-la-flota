/*
 * Copyright (C) 2013 Ignacio Calvo <ignacio.calvo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package tablero.modelo;

/**
 * Resultado de fase.
 * @author Ignacio Calvo <ignacio.calvo@gmail.com>
 */
public enum ResultadoFase {
    /**
     * La fase ha terminado, pero sigue el juego.
     */
    FASE_TERMINADA,
    /**
     * El juego ha terminado, aunque queden más fases después.
     */
    JUEGO_TERMINADO
}
