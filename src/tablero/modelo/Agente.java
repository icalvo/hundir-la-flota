/*
 * Copyright (C) 2013 Ignacio Calvo <ignacio.calvo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package tablero.modelo;

/**
 * 
 * @author Ignacio Calvo <ignacio.calvo@gmail.com>
 * @param <TJugada> Tipo de la jugada que devuelve el agente
 * @param <TJugador> Tipo de jugador asociado al agente
 * @param <TVista> Tipo de la vista que usa el agente para hacer su jugada
 */
public interface Agente<TJugada, TVista> {

    /**
     * 
     * @param vistaUniverso Vista que el agente tiene del universo para
     * determinar su jugada.
     * @return La nueva jugada, o null si el jugador abandona
     */
    TJugada nuevaJugada(TVista vistaUniverso);
    
    /**
     * Nombre del jugador asociado al agente
     * @return 
     */
    String getNombreJugador();

    /**
     * 
     * @param jugada
     * @param agente
     * @param vista 
     */
    void notificar(
            TJugada jugada, 
            Agente<TJugada, TVista> agente,
            TVista vista);
}
