/*
 * Copyright (C) 2013 Ignacio Calvo <ignacio.calvo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hundirlaflota.interfaz;

import hundirlaflota.modelo.Configuracion;
import hundirlaflota.modelo.tablero.Coordenada;
import hundirlaflota.modelo.tablero.Tablero;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.StringReader;
import java.nio.charset.Charset;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ignacio Calvo <ignacio.calvo@gmail.com>
 */
public class IOTest {
    
    public IOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        Configuracion.Cargar();
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of pedirCadena method, of class IO.
     */
    @Test
    public void testPedirCadena() throws Exception {
        System.out.println("pedirCadena");
        testPedirCadena("\n", "defecto");
        testPedirCadena("asdf\n", "asdf");
        testPedirCadena2("\n", null);
        testPedirCadena2("asdf\n", "asdf");
    }

    private void testPedirCadena(String entrada, String expResult) throws Exception {
        InputStream is = new ByteArrayInputStream(entrada.getBytes(Charset.defaultCharset()));
        OutputStream os = new ByteArrayOutputStream();
        System.setIn(is);
        System.setOut(new PrintStream(os));
        String msj = "Introduzca cadena";
        String valorDefecto = "defecto";
        String result = IO.pedirCadena(msj, valorDefecto);
        assertEquals(expResult, result);
        assertEquals("Introduzca cadena [defecto]: ", os.toString());
    }
    
    private void testPedirCadena2(String entrada, String expResult) throws Exception {
        InputStream is = new ByteArrayInputStream(entrada.getBytes(Charset.defaultCharset()));
        OutputStream os = new ByteArrayOutputStream();
        System.setIn(is);
        System.setOut(new PrintStream(os));
        String msj = "Introduzca cadena";
        String valorDefecto = null;
        String result = IO.pedirCadena(msj, valorDefecto);
        assertEquals(expResult, result);
        assertEquals("Introduzca cadena: ", os.toString());
    }    
    /**
     * Test of pedirEntero method, of class IO.
     */
    @Test
    public void testPedirEntero() throws Exception {
        testPedirEntero("\n", 12);
        testPedirEntero("asdf\n", 12);
        testPedirEntero("2467\n", 2467);
    }
    public void testPedirEntero(String entrada, int expResult) throws Exception {
        InputStream is = new ByteArrayInputStream(entrada.getBytes(Charset.defaultCharset()));
        OutputStream os = new ByteArrayOutputStream();
        System.setIn(is);
        System.setOut(new PrintStream(os));
        String msj = "Introduzca numero";
        int valorDefecto = 12;
        int result = IO.pedirEntero(msj, valorDefecto);
        assertEquals(expResult, result);
        assertEquals("Introduzca numero [12]: ", os.toString());
    }

    /**
     * Test of pedirCoordenada method, of class IO.
     */
    @Test
    public void testPedirCoordenada() throws Exception {
        testPedirCoordenada("A0\n", new Coordenada(0, 0));
        testPedirCoordenada("B4\n", new Coordenada(1, 4));
        testPedirCoordenada("4B\n", null);
    }

    public void testPedirCoordenada(String entrada, Coordenada expResult) throws Exception {
        InputStream is = new ByteArrayInputStream(entrada.getBytes(Charset.defaultCharset()));
        OutputStream os = new ByteArrayOutputStream();
        System.setIn(is);
        System.setOut(new PrintStream(os));
        String msj = "Introduzca coord";
        Coordenada valorDefecto = new Coordenada(1, 3);
        Coordenada result = IO.pedirCoordenada(msj, valorDefecto);
        assertEquals(expResult, result);
        assertEquals("Introduzca coord [B3]: ", os.toString());
    }

}