/*
 * Copyright (C) 2013 Ignacio Calvo <ignacio.calvo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hundirlaflota.modelo.agentes;

import hundirlaflota.interfaz.IO;
import hundirlaflota.modelo.tablero.Barco;
import hundirlaflota.modelo.tablero.Coordenada;
import hundirlaflota.modelo.Direccion;
import hundirlaflota.modelo.JugadorHundirLaFlota;
import hundirlaflota.modelo.tablero.TableroBarcos;
import hundirlaflota.modelo.tablero.TipoBarco;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import tablero.modelo.Agente;

/**
 *
 * @author Ignacio Calvo <ignacio.calvo@gmail.com>
 */
public class DisparadorPracticaTest {
    
    public DisparadorPracticaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * - Segundo tocado al sur (prueba norte-este-sur)
     * - Extremo del barco (no hay cambio de dirección)
     */
    @Test
    public void testNuevaJugada1() {
        System.out.println("nuevaJugada");
        TableroBarcos tableroEnemigo = new TableroBarcos(10, 10);
        VistaDisparador vista = new VistaDisparador(tableroEnemigo);
        ProveedorCoordenadasFijo prov = new ProveedorCoordenadasFijo();
        DisparadorPractica instance = new DisparadorPractica("TEST1", prov);

        tableroEnemigo.ponerBarco(
                new Barco(TipoBarco.PORTAAVIONES), 
                new Coordenada("B4"), Direccion.SUR);
        IO.mostrarTablero(System.out, tableroEnemigo);

        hacerJugada(prov, "A1", "A1", instance, vista, tableroEnemigo);
        hacerJugada(prov, "B4", "B4", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "A4", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "B5", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "C4", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "D4", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "E4", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "F4", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "G7", instance, vista, tableroEnemigo);
    }

    /**
     * - Segundo tocado al norte (prueba norte)
     * - Mitad del barco (hay cambio de dirección)
     */
    @Test
    public void testNuevaJugada2() {
        System.out.println("nuevaJugada");
        TableroBarcos tableroEnemigo = new TableroBarcos(10, 10);
        VistaDisparador vista = new VistaDisparador(tableroEnemigo);
        ProveedorCoordenadasFijo prov = new ProveedorCoordenadasFijo();
        DisparadorPractica instance = new DisparadorPractica("TEST1", prov);

        tableroEnemigo.ponerBarco(
                new Barco(TipoBarco.PORTAAVIONES), 
                new Coordenada("B4"), Direccion.SUR);
        IO.mostrarTablero(System.out, tableroEnemigo);

        hacerJugada(prov, "C4", "C4", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "B4", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "A4", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "D4", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "E4", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "F4", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "G7", instance, vista, tableroEnemigo);
    }
    

    /**
     * - Segundo tocado al oeste (prueba norte-este-sur-oeste)
     * - Extremo del barco (no hay cambio de dirección)
     */
    @Test
    public void testNuevaJugada3() {
        System.out.println("nuevaJugada");
        TableroBarcos tableroEnemigo = new TableroBarcos(10, 10);
        VistaDisparador vista = new VistaDisparador(tableroEnemigo);
        ProveedorCoordenadasFijo prov = new ProveedorCoordenadasFijo();
        DisparadorPractica instance = new DisparadorPractica("TEST1", prov);

        tableroEnemigo.ponerBarco(
                new Barco(TipoBarco.PORTAAVIONES), 
                new Coordenada("B1"), Direccion.ESTE);
        IO.mostrarTablero(System.out, tableroEnemigo);

        hacerJugada(prov, "B5", "B5", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "A5", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "B6", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "C5", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "B4", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "B3", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "B2", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "B1", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "G7", instance, vista, tableroEnemigo);
    }
    
    /**
     * - Segundo tocado al oeste, buque pegado al este (prueba norte-sur-oeste)
     * - Extremo del barco (no hay cambio de dirección)
     */
    @Test
    public void testNuevaJugada4() {
        System.out.println("nuevaJugada");
        TableroBarcos tableroEnemigo = new TableroBarcos(10, 10);
        VistaDisparador vista = new VistaDisparador(tableroEnemigo);
        ProveedorCoordenadasFijo prov = new ProveedorCoordenadasFijo();
        DisparadorPractica instance = new DisparadorPractica("TEST1", prov);

        tableroEnemigo.ponerBarco(
                new Barco(TipoBarco.PORTAAVIONES), 
                new Coordenada("B5"), Direccion.ESTE);
        IO.mostrarTablero(System.out, tableroEnemigo);

        hacerJugada(prov, "B9", "B9", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "A9", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "C9", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "B8", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "B7", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "B6", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "B5", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "G7", instance, vista, tableroEnemigo);
    }
        
    
    /**
     * - Segundo tocado al oeste, buque pegado al norte-este (prueba sur-oeste)
     * - Extremo del barco (no hay cambio de dirección)
     */
    @Test
    public void testNuevaJugada5() {
        System.out.println("nuevaJugada");
        TableroBarcos tableroEnemigo = new TableroBarcos(10, 10);
        VistaDisparador vista = new VistaDisparador(tableroEnemigo);
        ProveedorCoordenadasFijo prov = new ProveedorCoordenadasFijo();
        DisparadorPractica instance = new DisparadorPractica("TEST1", prov);

        tableroEnemigo.ponerBarco(
                new Barco(TipoBarco.PORTAAVIONES), 
                new Coordenada("A5"), Direccion.ESTE);
        IO.mostrarTablero(System.out, tableroEnemigo);

        hacerJugada(prov, "A9", "A9", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "B9", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "A8", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "A7", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "A6", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "A5", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "G7", instance, vista, tableroEnemigo);
    }


    /**
     * - Transición directa BARCO_LOCALIZADO_SIN_DIRECCION ->
     * BARCO_LOCALIZADO_CON_DIRECCION_INVERSA
     */
    @Test
    public void testNuevaJugada6() {
        System.out.println("nuevaJugada");
        TableroBarcos tableroEnemigo = new TableroBarcos(10, 10);
        VistaDisparador vista = new VistaDisparador(tableroEnemigo);
        ProveedorCoordenadasFijo prov = new ProveedorCoordenadasFijo();
        DisparadorPractica instance = new DisparadorPractica("TEST1", prov);

        tableroEnemigo.ponerBarco(
                new Barco(TipoBarco.PORTAAVIONES), 
                new Coordenada("A6"), Direccion.SUR);
        IO.mostrarTablero(System.out, tableroEnemigo);

        hacerJugada(prov, "B6", "B6", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "A6", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "C6", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "D6", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "E6", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "G7", instance, vista, tableroEnemigo);
    }
    


    /**
     * - Segundo tocado al sur (prueba norte-este-sur)
     * - Extremo del barco (no hay cambio de dirección)
     */
    @Test
    public void testNuevaJugada7() {
        System.out.println("nuevaJugada");
        TableroBarcos tableroEnemigo = new TableroBarcos(10, 10);
        VistaDisparador vista = new VistaDisparador(tableroEnemigo);
        ProveedorCoordenadasFijo prov = new ProveedorCoordenadasFijo();
        DisparadorPractica instance = new DisparadorPractica("TEST1", prov);

        tableroEnemigo.ponerBarco(
                new Barco(TipoBarco.PORTAAVIONES), 
                new Coordenada("B9"), Direccion.SUR);
        IO.mostrarTablero(System.out, tableroEnemigo);

        hacerJugada(prov, "B9", "B9", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "A9", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "C9", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "D9", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "E9", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "F9", instance, vista, tableroEnemigo);
        hacerJugada(prov, "G7", "G7", instance, vista, tableroEnemigo);
    }
        
    /**
     * Simula una jugada tal cual lo hace el framework, esto es: pide una
     * jugada al agente, la ejecuta y notifica el resultado. Para el
     * DisparadorPractica pasamos además su proveedor de coordenadas aleatorias
     * y la coordenada que dicho proveedor proporcionará en caso de que el
     * agente la pida.
     * @param prov Proveedor de coordenadas aleatorias
     * @param coordProv Coordenada para el proveedor de coordenadas
     * @param expected Jugada que el agente debe devolver
     * @param instance Agente
     * @param vista Vista del agente
     * @param tableroEnemigo Tablero de juego para imprimirlo
     */
    private void hacerJugada(
            ProveedorCoordenadasFijo prov,
            String coordProv, 
            String expected, 
            DisparadorPractica instance, 
            VistaDisparador vista, 
            TableroBarcos tableroEnemigo) {
        Coordenada expResult;
        Coordenada result;
        prov.setCoordenada(new Coordenada(coordProv));
        expResult = new Coordenada(expected);
        result = instance.nuevaJugada(vista);
        assertEquals(expResult, result);

        tableroEnemigo.disparar(result);
        instance.notificar(result, instance, vista);
        IO.mostrarTablero(System.out, tableroEnemigo);
    }
    /**
     * Test of notificar method, of class DisparadorPractica.
     */
    public void testNotificar() {
        System.out.println("notificar");
        Coordenada coord = null;
        Agente<Coordenada, VistaDisparador> agente = null;
        VistaDisparador vista = null;
        DisparadorPractica instance = null;
        instance.notificar(coord, agente, vista);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

}