/*
 * Copyright (C) 2013 Ignacio Calvo <ignacio.calvo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hundirlaflota.modelo;
import hundirlaflota.modelo.tablero.Coordenada;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ignacio Calvo <ignacio.calvo@gmail.com>
 */
public class CoordenadaTest {

    /**
     * Test of constructor with string, of class Coordenada.
     */
    @Test
    public void testCoordenada() {
        System.out.println("Coordenada(String)");
        Coordenada instance = new Coordenada("B6");
        Coordenada expResult = new Coordenada(1, 6);
        assertEquals(expResult.getX(), instance.getX());
        assertEquals(expResult.getY(), instance.getY());
    }
    
    /**
     * Test of getX method, of class Coordenada.
     */
    @Test
    public void testGetX() {
        System.out.println("getX");
        Coordenada instance = new Coordenada(3, 7);
        int expResult = 3;
        int result = instance.getX();
        assertEquals(expResult, result);
    }

    /**
     * Test of getY method, of class Coordenada.
     */
    @Test
    public void testGetY() {
        System.out.println("getY");
        Coordenada instance = new Coordenada(3, 7);
        int expResult = 7;
        int result = instance.getY();
        assertEquals(expResult, result);
    }
}