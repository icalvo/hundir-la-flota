/*
 * Copyright (C) 2013 Ignacio Calvo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hundirlaflota.modelo;

import hundirlaflota.modelo.tablero.TableroBarcos;
import java.util.Collection;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import tablero.modelo.Universo;

/**
 *
 * @author Ignacio Calvo
 */
public class JuegoHundirLaFlotaTest {
    
    public JuegoHundirLaFlotaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getUniverso method, of class JuegoHundirLaFlota.
     */
    @Test
    public void testGetUniverso() {
        System.out.println("getUniverso");
        JuegoHundirLaFlota instance = new JuegoHundirLaFlota();
        Universo expResult = null;
        Universo result = instance.getUniverso();
        assertNotNull(result);
    }

}