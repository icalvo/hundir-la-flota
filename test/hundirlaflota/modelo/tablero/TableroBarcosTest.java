/*
 * Copyright (C) 2013 Ignacio Calvo <ignacio.calvo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hundirlaflota.modelo.tablero;

import hundirlaflota.modelo.Direccion;
import hundirlaflota.modelo.ResultadoPonerBarco;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ignacio Calvo <ignacio.calvo@gmail.com>
 */
public class TableroBarcosTest {
    
    public TableroBarcosTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of ponerBarco method, of class TableroBarcos.
     */
    @Test
    public void testPonerBarco() {
        System.out.println("ponerBarco");

        Barco port, subm;
        TableroBarcos instance;
        ResultadoPonerBarco result1, result2;

        port = new Barco(TipoBarco.PORTAAVIONES);
        subm = new Barco(TipoBarco.SUBMARINO);
        instance = new TableroBarcos(10, 10);
        result1 = instance.ponerBarco(port, new Coordenada("H1"), Direccion.ESTE);
        result2 = instance.ponerBarco(subm, new Coordenada("G6"), Direccion.SUR);
        assertEquals(ResultadoPonerBarco.OK, result1);
        assertEquals(ResultadoPonerBarco.COLISION, result2);

        port = new Barco(TipoBarco.PORTAAVIONES);
        subm = new Barco(TipoBarco.SUBMARINO);
        instance = new TableroBarcos(10, 10);
        result1 = instance.ponerBarco(port, new Coordenada("H1"), Direccion.ESTE);
        result2 = instance.ponerBarco(subm, new Coordenada("H6"), Direccion.NORTE);
        assertEquals(ResultadoPonerBarco.OK, result1);
        assertEquals(ResultadoPonerBarco.COLISION, result2);
        
        port = new Barco(TipoBarco.PORTAAVIONES);
        subm = new Barco(TipoBarco.SUBMARINO);
        instance = new TableroBarcos(10, 10);
        result1 = instance.ponerBarco(port, new Coordenada("H5"), Direccion.OESTE);
        result2 = instance.ponerBarco(subm, new Coordenada("G6"), Direccion.SUR);
        assertEquals(ResultadoPonerBarco.OK, result1);
        assertEquals(ResultadoPonerBarco.COLISION, result2);

        port = new Barco(TipoBarco.PORTAAVIONES);
        subm = new Barco(TipoBarco.SUBMARINO);
        instance = new TableroBarcos(10, 10);
        result1 = instance.ponerBarco(port, new Coordenada("H5"), Direccion.OESTE);
        result2 = instance.ponerBarco(subm, new Coordenada("H6"), Direccion.NORTE);
        assertEquals(ResultadoPonerBarco.OK, result1);
        assertEquals(ResultadoPonerBarco.COLISION, result2);

        port = new Barco(TipoBarco.PORTAAVIONES);
        subm = new Barco(TipoBarco.SUBMARINO);
        instance = new TableroBarcos(10, 10);
        result2 = instance.ponerBarco(subm, new Coordenada("G6"), Direccion.SUR);
        result1 = instance.ponerBarco(port, new Coordenada("H1"), Direccion.ESTE);
        assertEquals(ResultadoPonerBarco.OK, result2);
        assertEquals(ResultadoPonerBarco.COLISION, result1);

        port = new Barco(TipoBarco.PORTAAVIONES);
        subm = new Barco(TipoBarco.SUBMARINO);
        instance = new TableroBarcos(10, 10);
        result2 = instance.ponerBarco(subm, new Coordenada("H6"), Direccion.NORTE);
        result1 = instance.ponerBarco(port, new Coordenada("H1"), Direccion.ESTE);
        assertEquals(ResultadoPonerBarco.OK, result2);
        assertEquals(ResultadoPonerBarco.COLISION, result1);
        
        port = new Barco(TipoBarco.PORTAAVIONES);
        subm = new Barco(TipoBarco.SUBMARINO);
        instance = new TableroBarcos(10, 10);
        result2 = instance.ponerBarco(subm, new Coordenada("G6"), Direccion.SUR);
        result1 = instance.ponerBarco(port, new Coordenada("H5"), Direccion.OESTE);
        assertEquals(ResultadoPonerBarco.OK, result2);
        assertEquals(ResultadoPonerBarco.COLISION, result1);

        port = new Barco(TipoBarco.PORTAAVIONES);
        subm = new Barco(TipoBarco.SUBMARINO);
        instance = new TableroBarcos(10, 10);
        result2 = instance.ponerBarco(subm, new Coordenada("H6"), Direccion.NORTE);
        result1 = instance.ponerBarco(port, new Coordenada("H5"), Direccion.OESTE);
        assertEquals(ResultadoPonerBarco.OK, result2);
        assertEquals(ResultadoPonerBarco.COLISION, result1);
        
        
    }

    /**
     * Test of ocupada method, of class TableroBarcos.
     */
    @Test
    public void testOcupada() {
        System.out.println("ocupada");
        Coordenada coord = new Coordenada("A4");
        TableroBarcos instance = new TableroBarcos(10, 10);
        boolean result = instance.ocupada(coord);
        assertFalse(result);
    }

    /**
     * Test of ocupada method, of class TableroBarcos.
     */
    @Test
    public void testOcupada2() {
        System.out.println("ocupada2");
        Coordenada coord = new Coordenada("A4");
        TableroBarcos instance = new TableroBarcos(10, 10);
        instance.ponerBarco(new Barco(TipoBarco.PORTAAVIONES), new Coordenada("A2"), Direccion.ESTE);
        boolean result = instance.ocupada(coord);
        assertTrue(result);
    }

    /**
     * Test of esCoordenadaValida method, of class TableroBarcos.
     */
    @Test
    public void testEsCoordenadaValida() {
        System.out.println("esCoordenadaValida");
        Coordenada coord = null;
        TableroBarcos instance = new TableroBarcos(10, 10);
        boolean result = instance.esCoordenadaValida(coord);
        assertTrue(instance.esCoordenadaValida(new Coordenada("A4")));
        assertTrue(instance.esCoordenadaValida(new Coordenada("A0")));
        assertTrue(instance.esCoordenadaValida(new Coordenada("J0")));
        assertTrue(instance.esCoordenadaValida(new Coordenada("A9")));
        assertTrue(instance.esCoordenadaValida(new Coordenada("J9")));

        assertFalse(instance.esCoordenadaValida(new Coordenada(-3, -2)));
        assertFalse(instance.esCoordenadaValida(new Coordenada(-3, 4)));
        assertFalse(instance.esCoordenadaValida(new Coordenada(-3, 13)));
        assertFalse(instance.esCoordenadaValida(new Coordenada(5, -2)));
        assertFalse(instance.esCoordenadaValida(new Coordenada(5, 13)));
        assertFalse(instance.esCoordenadaValida(new Coordenada(11, -2)));
        assertFalse(instance.esCoordenadaValida(new Coordenada(11, 4)));
        assertFalse(instance.esCoordenadaValida(new Coordenada(11, 13)));
    }

    /**
     * Test of getAlto method, of class TableroBarcos.
     */
    @Test
    public void testGetAlto() {
        System.out.println("getAlto");
        TableroBarcos instance = new TableroBarcos(12, 19);
        int expResult = 12;
        int result = instance.getAlto();
        assertEquals(expResult, result);
    }

    /**
     * Test of getAncho method, of class TableroBarcos.
     */
    @Test
    public void testGetAncho() {
        System.out.println("getAncho");
        TableroBarcos instance = new TableroBarcos(12, 19);
        int expResult = 19;
        int result = instance.getAncho();
        assertEquals(expResult, result);
    }

    /**
     * Test of disparar method, of class TableroBarcos.
     */
    @Test
    public void testDisparar() {
        System.out.println("disparar");
        TableroBarcos instance = new TableroBarcos(10, 10);
        boolean result;
        Coordenada jugada;

        instance.ponerBarco(new Barco(TipoBarco.SUBMARINO), new Coordenada("A1"), Direccion.ESTE);
        
        jugada = new Coordenada("A0");
        result = instance.disparar(jugada);
        assertEquals(true, result);
        assertEquals(TipoCasilla.AGUA, instance.casillaEn(jugada).getTipo());

        jugada = new Coordenada("A1");
        result = instance.disparar(jugada);
        assertEquals(true, result);
        assertEquals(TipoCasilla.TOCADO, instance.casillaEn(jugada).getTipo());

        jugada = new Coordenada("A2");
        result = instance.disparar(jugada);
        assertEquals(true, result);
        assertEquals(TipoCasilla.HUNDIDO, instance.casillaEn(jugada).getTipo());

        jugada = new Coordenada("A1");
        result = instance.disparar(jugada);
        assertEquals(false, result);

        jugada = new Coordenada("L8");
        result = instance.disparar(jugada);
        assertEquals(false, result);
        
    }

    /**
     * Test of vencido method, of class TableroBarcos.
     */
    @Test
    public void testVencido() {
        System.out.println("vencido");
        TableroBarcos instance = new TableroBarcos(10, 10);
        boolean result;
        Coordenada jugada;

        instance.ponerBarco(new Barco(TipoBarco.SUBMARINO), new Coordenada("A1"), Direccion.ESTE);
        
        jugada = new Coordenada("A0");
        result = instance.disparar(jugada);
        assertEquals(true, result);
        assertEquals(TipoCasilla.AGUA, instance.casillaEn(jugada).getTipo());
        assertEquals(false, instance.vencido());

        jugada = new Coordenada("A1");
        result = instance.disparar(jugada);
        assertEquals(true, result);
        assertEquals(TipoCasilla.TOCADO, instance.casillaEn(jugada).getTipo());
        assertEquals(false, instance.vencido());

        jugada = new Coordenada("A2");
        result = instance.disparar(jugada);
        assertEquals(true, result);
        assertEquals(TipoCasilla.HUNDIDO, instance.casillaEn(jugada).getTipo());
        assertEquals(true, instance.vencido());
        
    }

    /**
     * Test of getBarcos method, of class TableroBarcos.
     */
    @Test
    public void testGetBarcos() {
        System.out.println("getBarcos");
        TableroBarcos instance = new TableroBarcos(10, 10);
        ArrayList<Barco> result = instance.getBarcos();
        assertEquals(0, result.size());

        instance.ponerBarco(new Barco(TipoBarco.SUBMARINO), new Coordenada("A1"), Direccion.ESTE);
        assertEquals(1, result.size());
        assertEquals(TipoBarco.SUBMARINO, result.get(0).getTipo());

    }
}