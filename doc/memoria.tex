\documentclass[11pt,a4paper,titlepage,twoside]{article}

%% PAQUETES
%% --------
\usepackage{times} % necesario para generar caracteres reales
\usepackage[T1]{fontenc} % genera caracteres especiales reales (á en lugar de 'a)
\usepackage[utf8]{inputenc} % permite escribir caracteres especiales
\usepackage[spanish,activeacute]{babel} % guiones españoles
\usepackage{makeidx}

%% MARGENES
%% --------
\oddsidemargin  0cm
\evensidemargin 0cm
\textwidth      16cm
\textheight     22cm 
\topmargin      0cm

%% DOCUMENTO
%% ---------

\begin{document}
\title{Hundir la flota}
\author{Ignacio Calvo\footnote{E-mail: \texttt{ignacio.calvo@gmail.com}}}
\date{17 de junio de 2013}
\maketitle
\begin{abstract}
Memoria para la práctica de la asignatura de Lenguajes de Programación 
(Ingeniería Técnica Informática de Sistemas, UNED). La práctica consiste
en desarrollar el juego ``Hundir la flota'' en lenguaje Java.
\end{abstract}

\section{Introducción}

A la hora de diseñar el software, se decidió crear una capa que
abstrayera el funcionamiento general de los juegos por turnos. Esta capa
constituye, por tanto, un \emph{framework} para el desarrollo general de
este tipo de juegos, como pueda ser el ajedrez, las damas, juegos de
cartas, etc. En este \emph{framework} ya están implementados ciertos
procedimientos que ya no es necesario implementar, por tanto, cuando
pasamos a programar un juego concreto.

Esta capa ha sido alojada en el paquete \texttt{tablero.modelo},
mientras que la implementación particular para el juego ``Hundir la
flota'' puede hallarse en el paquete \texttt{hundirlaflota} y sus
subpaquetes.

Otra decisión ajena al enunciado de la práctica consistió en escribir
una suite con algunos tests unitarios y de integración. Esta suite
permite comprobar que cualquier modificación en el código no rompa nada
de lo anteriormente implementado, y sirve además como un modo de dejar
reflejado en el propio código la especificación de funcionamiento del
software.

A continuación vamos a detallar las decisiones diseño referentes al
\emph{framework} \texttt{tablero.modelo} y a su implementación concreta
de \texttt{hundirlaflota}.

\section{El \emph{framework} tablero.modelo}

\subsection{Juego}

Un \textbf{juego por turnos} viene representado por la clase abstracta
\texttt{Juego}. Esta clase tiene un método \texttt{jugar} ya
implementado, que implementa un juego completo. El método realiza las
siguientes acciones:

\begin{itemize}
\itemsep1pt\parskip0pt\parsep0pt
\item
  Crear el universo de juego, compuesto de jugadores y los objetos
  activos (p.~ej. el tablero, ver más adelante)
\item
  Obtener el iterador de fases (ver más adelante)
\item
  Por cada fase:
\item
  Ejecutamos la fase con su método \texttt{jugar}.
\item
  Si el resultado es que el juego ha terminado, salimos del bucle
\item
  Devolvemos los jugadores (así el llamador sabrá quién ha ganado)
\end{itemize}

Dada su concisión, mostramos aquí el código de \texttt{jugar} y podremos
comprobar que no tiene ninguna complejidad añadida a lo dicho:

\begin{verbatim}
public Collection<TJugador> jugar() {
    Universo<TJugador, TDatosJugador> universo;
    
    universo = getUniverso();
    Collection<TJugador> jugadores = universo.getJugadores();
    
    Collection<Fase> fases = this.getFases(universo);

    for (Fase fase: fases) {
        ResultadoFase resultado;
        System.out.println(fase.getNombre());
        resultado = fase.jugar();
        if (resultado == ResultadoFase.JUEGO_TERMINADO) {
            break;
        }
    } // for Fases
    
    return jugadores;       
}
\end{verbatim}

La clase \texttt{Juego} tiene dos parámetros genéricos: el tipo del
jugador, que debe implementar la interfaz \texttt{Jugador}, y el tipo de
datos asociados al jugador (que puede ser un tipo cualquiera).

Finalmente, la clase tiene dos métodos abstractos:

\begin{itemize}
\itemsep1pt\parskip0pt\parsep0pt
\item
  \texttt{getUniverso}, para crear el universo.
\item
  \texttt{getFases}, para obtener el conjunto de fases del juego.
\end{itemize}

\subsection{Jugadores}

El juego dispone de una serie de \textbf{jugadores}, que son quienes
deciden en cada momento lo que debe hacerse, de acuerdo a las normas. La
interfaz \texttt{Jugador} representa este concepto. La única función
obligatoria de esta interfaz es la de nombrar al jugador y gestionar su
eliminación. Sin embargo, es recomendable (ver
\texttt{JugadorHundirLaFlota} más adelante) implementar métodos para
obtener los distintos agentes que utiliza el jugador.

\subsection{Fases y agentes}

El juego se divide en un cierto número de \textbf{fases}, representadas
por la clase abstracta \texttt{Fase}. Cada fase se caracteriza por un
modo de operar distinto de los jugadores. Por ejemplo, el juego ``Hundir
la flota'' tiene dos fases diferentes: la fase de colocación de la flota
y la fase de disparos. En cada una los jugadores realizan jugadas
distintas. Para representar a un jugador dentro de una fase determinada
se emplea el concepto de \textbf{agente}.

El interfaz \texttt{Agente} tiene dos parámetros genéricos: el tipo de
jugada y el tipo de vista (ver más adelante).

El método principal de \texttt{Fase} es \texttt{jugar}, y es llamado por
la clase \texttt{Juego}. En este método se obtienen de cada jugador los
agentes adecuados a la fase y se entra en un bucle en el que se ejecutan
las siguientes acciones:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\itemsep1pt\parskip0pt\parsep0pt
\item
  Se obtiene el siguiente jugador al que le toca turno, y su
  correspondiente agente.
\item
  Se obtiene la vista del agente (ver más adelante), es decir un objeto
  que representa su punto de vista del universo de juego.
\item
  Se pide al agente su jugada (pasándole la vista).
\item
  Si la jugada es \texttt{null} se interpreta que abandona.
\item
  Se intenta aplicar la jugada al universo de juego.
\item
  Si es incorrecta (no se pudo aplicar), el jugador pierde su turno.
\item
  Si es correcta, se notifica a todos los agentes la jugada para que
  tengan constancia. También se notifica a la propia fase, mediante la
  función \texttt{depurar}, que puede emplearse para imprimir
  información.
\item
  Se examinan los jugadores no eliminados; si quedan uno o ninguno, se
  termina la fase y el juego (si queda uno, con un ganador, si no queda
  ninguno, con tablas); se sale del método con resultado
  \texttt{JUEGO\_TERMINADO}.
\item
  Se pregunta si la fase está terminada, en caso afirmativo se sale del
  método con resultado \texttt{FA\-SE\_TER\-MI\-NA\-DA}.
\end{enumerate}

Para implementar la clase \texttt{Fase}, los descendientes deben
implementar los siguientes métodos abstractos:

\begin{itemize}
\itemsep1pt\parskip0pt\parsep0pt
\item
  \texttt{siguienteJugador(TJugador)}: Devuelve el jugador al que le
  toca el turno a continuación.
\item
  \texttt{getAgente(TJugador)}: Devuelve el agente del jugador dado.
\item
  \texttt{getVista(TJugador)}: Devuelve la vista para el jugador actual.
\item
  \texttt{aplicarJugada(TJugada, TAgente)}: Aplica la jugada generada
  por el agente.
\item
  \texttt{eliminado(TAgente)}: Determina si el agente está eliminado
\item
  \texttt{depurar(TJugada, TJugador)}: Notifica a la fase de una jugada
  correcta.
\item
  \texttt{terminada(TAgente)}: Determina si la fase ha terminado
\item
  \texttt{getNombre()}: Obtiene el nombre de la fase
\end{itemize}

La interfaz \texttt{Agente}, por su lado, dispone de tres métodos:

\begin{itemize}
\itemsep1pt\parskip0pt\parsep0pt
\item
  \texttt{getNombreJugador()}: Devuelve el nombre de su jugador
  asociado.
\item
  \texttt{notificar(TJugada, TAgente, TVista)}: Notifica al agente de
  una jugada correcta hecha por otro agente o por él mismo. La vista
  pasada es la suya propia, por supuesto.
\item
  `nuevaJugada(TVista): Calcula una nueva jugada para la vista dada.
\end{itemize}

\subsection{Jugadas}

Los agentes y sus correspondientes fases emplean un tipo de
\textbf{jugada} representado por el parámetro genérico \texttt{TJugada},
aplicado a los tipos \texttt{Fase} y \texttt{Agente}. Dentro de una
fase, se pregunta al agente que tenga el turno su jugada, y después se
intenta aplicar la jugada correspondiente. No existe limitación para
estos tipos; no se les exige cumplir con ningún interfaz o heredar de
ninguna clase, dado que su origen (el agente) y su destino (la fase y en
última instancia el universo) saben cómo manejarlas.

\subsection{Universo}

Denominamos \textbf{universo} al conjunto de objetos sobre los que se
pueden aplicar jugadas (típicamente un tablero), más el conjunto de
jugadores. Para representarlo se emplea la clase abstracta
\texttt{Universo}, que tendrá como parámetros genéricos el tipo de
jugador y el tipo de conjunto de objeto (datos de cada jugador).

La clase permite asociar un conjunto de objetos diferente a cada
jugador. En caso de que el conjunto de objetos sea el mismo para todos,
como pasa con el ajedrez, se puede asociar el mismo objeto a distintos
jugadores. Por ejemplo, obsérvese el constructor de \texttt{Universo}
para este caso:

\begin{verbatim}
public Universo(
        TJugador jugador1,
        TJugador jugador2,
        TDatos tablero) {
    this.jugadores = new ArrayList<>();
    this.datosJugador = new ArrayList<>();
    jugadores.add(jugador1);
    jugadores.add(jugador2);
    datosJugador.add(tablero);
    datosJugador.add(tablero);
}
\end{verbatim}

En el \emph{array} de datos de jugador hay 2 elementos (debe ser la
misma cantidad que de jugadores), pero ambos elementos referencian al
mismo objeto, el tablero de ajedrez.

\subsection{Vistas}

Es importante representar el \textbf{punto de vista} que cada jugador
tiene del universo. En juegos como el ajedrez, este punto de vista es
equivalente para ambos jugadores, pero en juegos como ``Hundir la
flota'', es diferente, puesto que cada jugador conoce en su totalidad
sólo su tablero, mientras que del del enemigo sólo conocemos los
disparos realizados y su resultado. El punto de vista se representa
mediante el parámetro genérico \texttt{TVista}, aplicado a los tipos
\texttt{Fase} y \texttt{Agente}. Cada fase es capaz de obtener la vista
actual del universo para cada jugador.

\section{hundirlaflota}

Ahora vamos a examinar cómo se han implementado los conceptos del
\emph{framework} descrito para el caso particular del juego ``Hundir la
flota''.

\subsection{Clases de datos}

Para representar los datos del juego se emplea una estructura en tres
niveles: Tablero, Casilla y Barco. El Tablero es un diccionario cuya
clave es una coordenada y su valor es una casilla. Además permite
determinar si una coordenada determinada es válida. La Casilla guarda la
información sobre su tipo (Sin disparar, agua, tocado o hundido) y el
barco asociado, si lo hay. El Barco tiene un tipo (tamaño) y una
colección de casillas que cubre el barco.

\texttt{Tablero} y \texttt{Casilla} son clases abstractas. Ambas se
implementan mediante sendas jerarquías paralelas:
\texttt{TableroBarcos}, \texttt{TableroOculto} y
\texttt{TableroSoloLectura}; y \texttt{CasillaBarcos},
\texttt{CasillaOculta} y \texttt{CasillaSoloLectura}.

El par \texttt{TableroBarcos}-\texttt{CasillaBarcos} implementa un
tablero con todos los datos visibles y que además se puede modificar
mediante los métodos \texttt{ponerBarco} y \texttt{disparar}. Estas
clases se emplean para representar el universo ``real'' de juego. A esta
instancia concreta no tienen acceso los agentes, de modo que no sea
posible implementar un agente que corrompa el estado del tablero. Sólo
las fases pueden, mediante sus implementaciones de
\texttt{aplicarJugada}, afectar directamente a estas instancias.

El par \texttt{TableroSoloLectura}-\texttt{CasillaSoloLectura}
implementa un tablero con todos los datos visibles pero no modificable.
Estas clases son empleadas para dar a los agentes una representación de
su propio tablero, impidiéndoles modificarlo.

Finalmente, el par \texttt{TableroOculto}-\texttt{CasillaOculta}
implementa un tablero en el que sólo se puede ver el tipo de las
casillas, pero no el barco asociado; y no modificable. Estas clases son
empleadas para dar a los agentes una representación del tablero enemigo.

Las versiones de sólo lectura y ocultas implementan el patrón de diseño
Delegado (\emph{Proxy}), pues emplean internamente un objeto de tipo
\texttt{Tablero} o \texttt{Casilla}, al cual consultan para implementar
sus métodos. Para construirlos, se les pasa el tablero ``real'' del
universo como parámetro.

\subsection{Juego}

El juego se implementa con \texttt{JuegoHundirLaFlota}. Implementamos
\texttt{getUniverso} creando los tableros, los jugadores (cada uno con
sus agentes para cada fase), y finalmente el universo. \texttt{getFases}
crea una colección con una instancia de cada fase, que veremos más
adelante.

Existe otra implementación llamada \texttt{JuegoHundirLaFlotaAuto}, que
crea dos jugadores artificiales (con \texttt{ColocadorSimple} y
\texttt{DisparadorPractica}). Esta implementación es usada para
demostrar automáticamente el juego.

\subsection{Jugadores}

Cada jugador tiene nombre y dos agentes, uno por cada fase.

\subsection{Fases}

``Hundir la flota'' se divide en dos fases. La primera es la fase de
colocación de flotas, implementada mediante la clase
\texttt{ColocacionFlotas} y la segunda es el juego propiamente dicho
donde los jugadores se disparan, implementada con la clase
\texttt{Disparos}.

\subsubsection{Colocación flotas}

Un colocador es un agente que devuelve jugadas de tipo \texttt{Flota} y
cuya vista es la \texttt{Vis\-ta\-Co\-lo\-ca\-dor}. El tipo \texttt{Flota}
representa una flota de buques, cada uno de ellos colocado. En la
práctica se proporcionan dos implementaciones:
\texttt{ColocadorAleatorio}, que dispone los buques de manera aleatoria,
y \texttt{ColocadorHumano}, que pregunta al usuario por la colocación de
cada buque.

La \texttt{VistaColocador} consta de un ancho y alto de tablero y de una
\texttt{Flota} ya creada, sin colocar, que el agente puede emplear
directamente para rellenarla y devolverla. Se recomienda que el agente
cree un nuevo \texttt{TableroBarcos} y lo emplee como método de
validación (\texttt{ponerBarco} sólo coloca el barco si se cumplen las
normas, y devuelve un resultado indicando el problema si no se pudo
colocar).

\subsubsection{Disparos}

Un disparador es un agente que devuelve jugadas de tipo
\texttt{Coordenada} y cuya vista es la \texttt{Vis\-ta\-Dis\-pa\-ra\-dor}. En la
práctica se han implementado tres versiones:
\texttt{DisparadorAleatorio}, que dispara aleatoriamente,
\texttt{DisparadorHumano}, que pide al usuario la coordenada siguiente,
y \texttt{Dis\-pa\-ra\-dor\-Prac\-ti\-ca}, que cumple las reglas establecidas en el
enunciado de la práctica.

La \texttt{VistaDisparador} consta de un \texttt{TableroSoloLectura}
representando el tablero propio y de un \texttt{TableroOculto}
representando el tablero enemigo.

\subsection{DisparadorPractica}

Vamos a analizar con detalle este agente puesto que es el que encapsula
la inteligencia artificial pedida por el enunciado.

Este disparador se expresa mediante un autómata determinista de cuatro
estados, que son:

\begin{itemize}
\itemsep1pt\parskip0pt\parsep0pt
\item
  \texttt{SIN\_BARCO\_LOCALIZADO}: El disparador no tiene barco
  localizado
\item
  \texttt{BARCO\_LOCALIZADO\_SIN\_DIRECCION}: El disparador ha
  localizado un barco pero no sabe en qué dirección
  (horizontal-vertical) se encuentra
\item
  \texttt{BARCO\_LOCALIZADO\_CON\_DIRECCION}: El disparador ha
  localizado un barco, sabe su dirección y lo está hundiendo en el
  primer sentido que ha encontrado.
\item
  \texttt{BARCO\_LOCALIZADO\_CON\_DIRECCION\_INVERSA}: El disparador ha
  localizado un barco, sabe su dirección y lo está hundiendo en el
  sentido inverso al primero que ha encontrado.
\end{itemize}

Asimismo necesitamos tres marcadores para poder determinar el cambio de
estado, que son:

\begin{itemize}
\itemsep1pt\parskip0pt\parsep0pt
\item
  \texttt{Coordenada primerTocado}: Primera casilla tocada en un barco
\item
  \texttt{Coordenada cabeza}: Última casilla tocada
\item
  \texttt{Direccion direccion}: Último sentido investigado
\end{itemize}

La lógica de \texttt{DisparadorPractica} se divide entre la
implementación de \texttt{nuevaJugada} y \texttt{notificar}. En
\texttt{nuevaJugada} devolvemos una nueva jugada aleatoria en caso de
estar en el estado \texttt{SIN\_BARCO\_LOCALIZADO}; en cualquier otro
estado siempre tendremos una dirección en la que probar y una última
coordenada tocada, por lo que simplemente disparamos una casilla más
adelante.

En \texttt{notificar} es donde realmente se implementa la lógica del
autómata, adoptándose la estructura clásica de un \texttt{switch} sobre
el estado actual y luego, en cada rama, otro \texttt{switch} sobre la
variable que determina las acciones a realizar, en este caso el
resultado de la última jugada.

\section{Interfaz de usuario}

Hay unas cuantas clases, ubicadas en los espacios de nombres
\texttt{hundirlaflota.interfaz.*}, cuyo cometido es la presentación de
datos y menús y la lectura de datos de la consola.

La clase \texttt{IO} contiene las operaciones básicas de entrada y salida
para la aplicación: mostrar tableros, pedir cadenas, pedir enteros y 
pedir coordenadas.

La clase abstracta \texttt{Menu} representa un menú con opciones, mientras
que la clase abstracta \texttt{AccionMenu} un ítem de menú ejecutable, y
cuenta también con un método para saber cuál es el menú de destino una
vez terminada la ejecución.

\section{Instrucciones}

\subsection{Estructura}

El fichero comprimido contiene un proyecto realizado con NetBeans 7.3.
La estructura es la siguiente:

\begin{itemize}
\itemsep1pt\parskip0pt\parsep0pt
\item
  \texttt{/build}: Ficheros compilados \texttt{.class}.
\item
  \texttt{/dist}: Ficheros \texttt{.jar} (empaquetado del directorio
  \texttt{/build}), y documentación javadoc generada.
\item
  \texttt{/doc}: Carpeta de documentación (contiene el código fuente \LaTeX de la memoria)
\item
  \texttt{/nbproject}: Configuración del entorno NetBeans
\item
  \texttt{/src}: Código fuente del proyecto
\item
  \texttt{/test}: Código fuente de test
\end{itemize}

Por lo tanto, el código fuente de la práctica es el contenido en
\texttt{/src} y sus subcarpetas.

\subsection{Ejecución}

Para ejecutarlo, puede emplearse el script \texttt{HundirLaFlota.cmd}
situado en el directorio raíz.

El programa presentará un menú con las siguientes opciones:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\itemsep1pt\parskip0pt\parsep0pt
\item
  \textbf{Configurar:} permite cambiar el ancho y el alto del tablero y
  la configuración de flota.
\item
  \textbf{Jugar:} Lanza una partida con un jugador humano y otro
  artificial de acuerdo a las especificaciones de la práctica.
\item
  \textbf{Demostración:} Lanza una partida con ambos jugadores
  artificiales de acuerdo a las especificaciones de la práctica. Se
  imprimen todos los tableros en cada jugada. De esta forma es posible
  observar los pasos de una partida completa. El programa pide una
  \textbf{semilla aleatoria} para generar los valores aleatorios de la
  partida. De este modo es posible reproducir una partida concreta
  introduciendo su número de semilla.
\item
  \textbf{Salir:} Sale del programa.
\end{enumerate}

\section{Conclusiones}

En mi caso particular, ya llevo muchos años programando profesionalmente
en el paradigma de orientación a objetos. Dentro de los lenguajes de
programación es quizá el paradigma que me parece más versátil y escalable,
sobre todo después de leer el ``Construcción de software orientado a objetos'',
de Meyer, que para mí supuso toda una revelación.

No podía por tanto contentarme con una implementación ``directa'' y por eso
decidí construir este pequeño \emph{framework}, que puede servir de base
para cualquier otro juego de tablero. La orientación a objetos es especialmente
buena para este tipo de tareas de abstracción.

Quizá la mayor dificultad está en escribir el código lo más claro y legible
posible, lo cual implica reorganizar con frecuencia. El concepto moderno de
``refactorización'' ayuda mucho, junto con las baterías de test unitario y
de integración, que dan confianza al programador a la hora de probar nuevos
enfoques.

Lo más complejo de la práctica es la programación del agente disparador
artificial especificado. El hecho de usar un autómata de estados da una
idea de esta complejidad, pues esta construcción sólo suele aparecer cuando
la estructura no se puede simplificar demasiado; además de dar lugar a funciones
bastante largas.

\newpage
\tableofcontents

\end{document}
